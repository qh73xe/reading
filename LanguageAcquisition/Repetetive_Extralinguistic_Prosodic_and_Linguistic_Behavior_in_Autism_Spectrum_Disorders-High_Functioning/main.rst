###################################################################################################################
Repetetive Extralinguistic, Prosodic and Linguistic Behavior in Autism Spectrum Disorders-High Functioning (ASD-HF)
###################################################################################################################

- Hila Green and Yishai Tobin
- Ben-Gurion University of the Negev,
- Israel

1.Introduction
===============

Restricted repetitive behavior has been a defining feature of the Autism Spectrum Disorders (ASD) since the original description of autism (Kanner, 1943), 
and by diagnostic convention, all individuals with ASD display some form of these “restricted repetitive and stereotyped patterns of behavior, 
interests, and activities” (Diagnostic and Statistical Manual for Mental Disorders-Forth Edition [DSM-IV], American Psychiatric, [APA], 1994:71). 
Although ASD is associated with a wide range of specific forms of atypical repetition, this issue received far less research attention than social and communication deficits. 
Indeed, it was not our original attention to examine the prosody of ASD high functioning (ASD-HF) children from the perspective of the presence or the absence of repetitive behavior, we were concentrating on “prosody” within the context of linguistic behavior - whether or not the manifestation of the “different” prosody by ASD-HF individuals may reflect “delays and deficits in language and communication”, which is another core feature of ASD. 
However, the data we collected in our research brought this issue into focus and raised new questions regarding the centrality of the restricted repetitive behaviors in ASD.

.. note:: 

   制限させた繰り返し行動は自閉症の原記述以来, 自閉症スペクトラム障害 (ASD) の決定的な特徴である [1]_ .
   また, diagnostic conventionによると, ASD をもつすべての個人は "制限された反復的で恒常的な, 行動, 興味, 活性度のパターン" 
   (Diagnostic and Statistical Manual for Mental Disorders-Forth Edition [DSM-IV], American Psychiatric, [APA], 1994:71) を示す.
   一方で, ADS は特定の形態の広範囲の非定型の繰り返しに関連しており, この問題は社会的, コミュニケーションの欠如よりも研究の興味が少ないように受け取られている.
   確かに, 我々の元々の興味は 高機能 ASD (ASD-HF) の子供の韻律情報の研究を存在の視点や繰り返し行動の欠如から行うことではなかったが,
   我々は言語行動の文脈の中で、" 韻律 " に注目した. 
   つまり, ASD-HF の個人の " 異なった " 韻律の表出は "言語やコミュニケーションの遅延や欠落" もしくは他の ASD の主要な機能を反映しているのか否かを調べた.
   
   しかし、我々の研究で収集されたデータは、上記の集中に関する問題を持ち込み、ASD制限反復行動の中心性に関する新たな疑問を提起した。

This chapter is based on results and insights from linguistic research. 
This research (Green, 2010) comparing and contrasting the prosodic features of 20 peer-matched 9-13 year old male Israeli Hebrew-speaking participants (10 ASD-HF subjects and 10 controls without developmental disorders (WDD) strongly indicated that the prosodic features that were examined exhibited a limited and repetitive repertoire in the ASD-HF population compared with the prosodic features of the WDD control population (Green, 2005; Green & Tobin 2008a, b, 2009 a, b, c; Green, 2010). 
Furthermore, this significant limited repetitive repertoire of behavior patterns was also exhibited in the extra-linguistic and the linguistic (lexical) domains of the ASD-HF participants.

.. note:: 

   本節は言語学的研究の結果と洞察に基づくものである.
   この研究 ( Green, 2010 ) では 
   9-13 歳のイスラエルのヘブライ語圏男性参加者にペアマッチする20個の韻律的特徴量を比較,対比している(10名の ASD-HF 患者と10名の発達障害を持っていないコントロール(WDD) 
   では ASD-HF群とWDDのコントロール群の韻律的特徴の比較において
   繰り返しのレパートリーと制限された韻律特徴が強く指し示された (Green, 2005; Green & Tobin 2008a, b, 2009 a, b, c; Green, 2010). 
   加えて, この重要な制限された繰り返し行動パターンのレパートリーはASD-HF参加者の非言語的·言語的（字句）ドメインでも出現した.

2.The experimental research
============================

As already noted, this chapter is based on experimental research and deals with the
“restricted repetitive behavior” phenomenon. 
In the original linguistic-oriented research
there were four major goals:

1. To describe, compare and contrast the phonetic realization of the fundamental frequency and the prosodic features of intonation in the language of children with ASD- HF and WDD children,
2. To establish a methodology which allows the analysis of more than one feature of prosody simultaneously,
3. To make use of instrumental measurements, (using recently developed speech technology tools) as well as perceptual analysis, and
4. To explain the results within the context of the theory of Phonology as Human Behavior (PHB) (e.g. Diver, 1979, 1995; Tobin, 1997, 2009), a linguistic theory which declares that:
    a. Language is a symbolic tool, whose structure is shaped both by its communicative function and the characteristics of its users (Tobin, 1990, 1993, 1994, 2009), and 
    b.  Language represents a compromise in the struggle to achieve maximum communication using minimal effort as presented in the theory of Phonology as Human Behavior (PHB) (Diver, 1979,1995; Tobin, 1997, 2009).

Our empirical data were drawn from the speech samples of 20 children between the ages 9- 13 years, in two main groups:

a. Research group: subjects diagnosed clinically with ASD-HF (N=10)
b. Control group: participants without developmental disorders (WDD, N=10)

The research group includes ten children with ASD aged 9-13 years. They were recruited from mainstream schools that have special education classes for children with ASD. The ASD diagnosis was made by a child psychiatrist who determined that the child met the DSM-IV, APA (1994) criteria for autism. Each child's special needs were discussed and defined by an "Evaluation Committee", entrusted with the placement of special needs pupils in appropriate class settings. For all of the children in this group the committee determined that a special class for children within the ASD spectrum is required. IQ scores were re- assessed by the school psychologist within the current year using the Wechsler Intelligence Scale for Children - a Revised Edition [WISC-R]. For the purposes of this research, High Functioning is defined by an IQ 85 and above. All ASD subject‘s have typical, within the average, school performance in the mainstream class in language and reading, as reported by their teachers.

The control group was composed of children without developmental disorders (WDD) and was drawn from the same schools as the research group. Similar to the research group subjects, in their teachers' judgment, all the children are average students and do not exhibit any particular academic difficulties or exceptional abilities. The group members have not been tested to determine their IQ scores, but from the information received in interviews with the teachers and parents it can be assumed that they have intelligence in normal range.  Their parents report that they have not been referred to a specialist for any developmental reasons. In our study, two language measures were used for peer matching. In addition to similar chronological ages (within two month), the peers were matched on the basis of (a) language fluency in spontaneous-speech sample as measured in MLU-W (within the minimal linguistic unit of one word) and (b) the standardized score of the verbal part in the IQ test within the norm or above. Match between subjects and controls are presented in Table 1.

The analysis of the speech samples of this group provides the basis for the characterization of the prosodic features of Israeli Hebrew (Green, 2009a; Green & Tobin 2009b, c; Green, 2010). All participants were male and at least second generation Israeli-born, and were monolingual speakers of Israeli Hebrew (IH). All participants were from comparable socioeconomic backgrounds and attend mainstream schools. Their mothers all have at least 12 years of education, an indication of socioeconomic status, since maternal education level is the most significant predictor of language functioning in children (Dollaghan et al., 1999).  None of the members of the participants' immediate families has learning or other known disabilities.


                   ASD-HF subjects                                   WDD control group
 Research                                                  Control
 Group                                            MLU      Group                              MLU-
 ASD-HF          Mo       VIQ     PIQ    Age      -W       WDD             Mo        Age      W
 1-ADR           13       109     94     9:0      5.62     11-AVY          13        9:0      5.28
 2-ITE           17       121     91     9:2      5.34     12-NIS          15        9:3      4.04
 3-UDX           15       108     94     10:8     7.86     13-IDR          17        10:7     6.94
 4-YOL           17       111     101    11:1     5.77     14-YVO          16        10:11    5.16
 5-RAE           12       86      97     11:6     5.68     15-ITS          17        11:6     4.04
 6-BAB           17       109     97     11:11    7.42     16-AVS          16        11:9     7.18
 7-ETR           16       100     102    12:5     5.14     17-LIS          13        12:3     5.22
 8-TOB           15       108     99     12:8     6.42     18-IDW          16        12:6     6.20
 9-NOR           14       90      99     13:0     6.5      19-OMX          14        12:11    5.86
 10-OMG(*)       14       89      85     13:0     4.8      20-IDS          17        12:11    6.1

Mo=Mother’s years of Education, VIQ/PIQ= verbal/performance Intelligence score (WISC-R) (*)
Participants 10 and 20 do not meet the requirements of the definitions used for peer matching, and are
consequently excluded from comparison between the groups. Their results are, however, included
when the discussion is about differences within the group.

Table 1. Matched peers and subject’s characteristics

The speech samples were collected at the participant's house, in his own room. There were three types of elicitation tasks: (a) Repetition: this task comprised four sentence pairs, a WH- Question and its answer, (b) Reading Aloud: participants were asked to read a short story, and (c) Spontaneous speech: these were elicited spontaneous speech sequences in response to open questions, relevant to the child's daily life.  In order to conduct acoustic analyses the speech files were digitized at a rate of 44.1KHz with 16-bit resolution, directly into a laptop computer (Hp Compaq 6710b), using the speech- recording software Audacity (a software package for recording and editing sound files) and a small microphone. The data were subsequently analyzed using the speech analysis program Pratt, version 5.0.30 (Computer program, from http://www.praat.org/). Scripts were written to extract data from the transcriptions. Script is a short program that is used to automate Pratt activities and enables the analysis of large data sets, quick processing of information and results, preparation for the use of simple statistics tools, and generation of summary information for control purposes, i.e. to identify errors in the manual transcription process.

3.Restricted repetitive behavior
================================

Restricted repetitive behaviors are a heterogeneous group of behaviors and a wide range of specific forms of atypical repetition that have been identified and described with relation to ASD (e.g. APA, 1994; Bodfish et al., 2000; Esbensen et al., 2009; Kanner 1943; Lewis & Bodfish, 1998; Militerni et al., 2002; Richler et al., 2007; Rutter, 1996; Szatmari et al., 2006; Turner, 1999). This restricted repetitive behavior can be observed across individuals with ASD, and multiple categories of abnormal repetition can occur within the individual with autism (e.g. Lewis & Bodfish, 1998; Wing & Gould, 1979). These behaviors can be socially inappropriate, increase the plausibility of living in a more restricted environment, and stigmatizing (Bonadonna, 1981; Durand & Carr 1987, Varni et al., 1979).

Several researchers who examined age related aspects of repetitive behavior patterns in ASD suggested that age and level of functioning are associated with variation in the manifestation of restricted repetitive behaviors in individuals with ASD (e.g. Esbensen et al., 2009; Militerni et al., 2002; Lam & Aman, 2007). The overall severity of the ASD has been shown to be significantly positively correlated with the overall severity of repetitive behaviors (e.g. Campbell et al., 1990; Prior & MacMillan, 1973). Esbensen et al. (2009) examined the restricted repetitive behaviors among a large group of children and adults with ASD in order to describe age related patterns of symptom expression and examine if age related patterns are different for the various types of restricted repetitive behaviors. In this research, they combined data from several previous studies to have a large sample size (n = 712), spanning a broad age range (age 2–62), and they measured restricted repetitive behaviors using a single instrument, the Repetitive Behavior Scale-Revised (RBS-R: Bodfish et al., 2000) with the modification of the subscales (Lam & Aman, 2007). The empirically derived subscales include: Stereotyped Behavior (movements with no obvious purpose that are repeated in a similar manner), Self-injurious Behavior (actions that cause or have the potential to cause redness, bruising, or other injury to the body), Compulsive Behavior (behavior that is repeated and performed according to a rule or involves things being done ‘‘just so’’), Ritualistic/sameness Behavior (performing activities of daily living in a similar manner; resistance to change, insisting that things stay the same), and Restricted Interests (limited range of focus, interest, or activity). Their analyses suggest that repetitive behaviors are less frequent and less severe among older individuals than among younger individuals regardless of whether examining total display of restricted repetitive behaviors, or whether examining each of the various subtypes. One may ask whether restricted repetitive behaviors decrease with age or whether they merely take a different form. A thought previously arise by Piven et al. (1996). Piven’s idea was that manifestation of ASD changes as the individual develops.

Other research has suggested that the expression of restricted repetitive behaviors may be influenced by level of functioning (e.g. Bartak & Rutter, 1976; Campbell et al., 1990; Gabriels et al., 2005; Le Couteur et al., 2003; Turner, 1999). Low IQ or presence of mental retardation has been shown to be associated with increased occurrence of repetitive behaviors in autism including stereotypy and self-injury (Bartak & Rutter 1976; Campbell et al., 1990).  Turner (1997) proposed a taxonomy of repetitive behavior; consisting of eleven categories and in a later review (Turner, 1999) suggested that human repetitive behaviors can be divided into (a) lower-level and (b) higher-level categories. Lower-level repetitive behaviors include dyskinesia (involuntary, repetitive movements), tics, repetitive manipulation of objects, repetitive forms of self-injurious behavior and stereotyped movements. Turner's review indicates that although some stereotyped movements and repetitive manipulation of objects might be differentiating features of autism, there are some lower-level repetitive behaviors that may rather be related to ability level or the presence of organic pathology (e.g. Bishop et al., 2006; Bodfish et al., 2000; Cuccaro et al., 2003; Esbensen et al., 2009; Fecteau et al., 2003; Militerni et al., 2002; Lam & Aman, 2007; Szatmari et al., 2006).  Irrespective of whether these low-level repetitive behavioral characteristics are unique to ASD or exist in a wider range of organic pathological conditions, they are all repetitive extra-linguistic behaviors.

The high-level repetitive behaviors include circumscribed interests, attachments to objects, insistence on maintenance of sameness and repetitive language. Turner (1999) suggested that certain types of higher-level behavior may be characteristic of and restricted to individuals with ASD once a certain level of development has been achieved.

3.1 Repetitive language behavior
--------------------------------

During the data analysis phase we could not ignore the proliferation of word repetition and repetition of contents. Repetitive language behavior has been reported in the literature (e.g.  Perkins et al., 2006), but as far as we can determine there has not been a comprehensive study of questions raised by this phenomenon.  The following is an example of the lexical repetition found in the spontaneous speech of BAB-ASD (age 11:11) regarding his “interest” (hitanyenut) in the “sciences” (mada‘im). The data are taken from sequential utterances in the same short conversation:

 U3:      [ ani mi# hahit‘anyenut sheli be‘ika(r) mada‘im ]
          I from my INTEREST ESPECIALLY LIKE SCIENCE
 U4:      [ hit‘anyenti bemada‘im kvar begil ca‘r ]
          I was INTERESTED in SCIENCE since I was young
 U5:      [ meod ahavti mada‘im ]
          I LIKED very much SCIENCE
 U6:      [mada‘im # shama‘ati shemada‘im # ze ha‘olam shemisvivenu]
          SCIENCE—I heard that SCIENCE is the world around us
 U13:     [ ani ohev et kol hamikco‘ot aval be‘iqar mada‘im ]
          I LIKE all the subjects BUT ESPECIALLY SCIENCE
 U14:     [ be‘iqar mada‘im ]
          ESPECIALLY SCIENCE
 U15:     [ ani yoter beqeTa shel mada‘im ]
          I am more into SCIENCE

3.2 Repetitive prosodic behavior
--------------------------------

The term ‘prosody’ is derived from the Greek ‘prosodia ’, which is a musical term.  Metaphorically, in linguistic contexts, it is implied that prosody is the musical accompaniment to the words themselves. The term “prosody” describes the way one says a particular utterance and covers a wide range of phenomena including: intonation patterns, stress and accent, and pauses and junctions, etc. in speech.

Atypical prosody have been reported in a wide range of developmental conditions including dysarthria (e.g. Brewester, 1989; Crystal, 1979; Vance, 1994), aphasia (e.g. Bryan, 1989; Cooper & Klouda, 1987; Moen 2009), in hearing impairment (e.g. Parkhurst & levitt, 1978; Monsen, 1983; Most & Peled, 2007), in developmental speech and language disorders and/or learning disabilities (e.g. Garken & McGregor, 1989; Hargrove, 1997; Hargrove & McGarr, 1994; Wells & Peppé, 2003), in Williams Syndrome e.g. Setter et al., 2007; Stjanovik et al., 2007), and in ASD.

In ASD the atypical prosody has been identified as core feature and since the initial description, by Kanner (1943) and Asperger (1944, as cited in Frith 1991), the "unnatural" prosody was marked using different narrations such as "monotonous", "odd", "sing-song", "exaggerated", and more. Asperger, translated in Frith (1991) wrote: "Sometimes the voice is soft and far way, sometimes it sounds refined and nasal but sometimes it is too shrill and ear- splitting. In yet other cases, the voice drones on in a sing-song and does not even go down at the end of sentence. However many possibilities there are, they all have one thing in common: the language feels unnatural" (Frith, 1991:70)

Research on prosody within the ASD population, has shown that even when other aspects of language improve, prosodic deficits tend to be persistent and show little change over time (e.g. Kanner, 1971; Simmons & Baltaxe, 1975). This persistence of prosodic deficits seems to limit the social acceptance of children with ASD-HF mainstreamed into the larger community since they sound strange to their peers (McCann & Peppé, 2003; Paul et al., 2001).

Adapting Fujisaki’s definition, “Prosody is the systematic organization of various linguistic units into an utterance or coherent group of utterances in the process of speech production.  Its realization involves both segmental and suprasegmental feature of speech and serves to convey not only linguistic information, but also paralinguistic and non-linguistic information” (Fujisaki, 1997:28). By this definition, Fujisaki established the prosodic features by two major components that can be measured: (a) the word accent, (b) the intonation, and they are both manifested by the contour of the voice F0 (the frequency of the vibration of the vocal folds). Hence, in order to understand the results and the insights from the presented research, we will first explore the nature of these two components from both a conceptual and operative view.

3.2.1 Word accent and the intonation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Bolinger (1958) formulates the relations of stress-accent. He argues that the main means to express stress is pitch and proposed the term accent for prominence in the utterance.  Following Bolinger, Pierrehumbert (1980) represents the F0 contour as a linear sequence of phonologically distinctive units - pitch accents and edge tones. The occurrence of these features within the sequence can be described linguistically as a grammar, within the Autosegmental-Metrical (AM) theory (Ladd, 1996; Liberman & Pierrehumbert, 1984; Pierrehumbert, 1980; Pierrehumbert & Hirschberg, 1990).

The AM theory is a generative phonological framework in which the tone is specified using an independent string of tonal segments and the prosody of an utterance is viewed as a hierarchically organized structure of phonologically defined features. Following the AM theory, Pierrehumbert (1980) proposes a description of intonation that consists of three parts:

1. The grammar of phrasal tones,
2. The metrical representation of the text,
3. The rules of assigning association lines.

Pierrehumbert assumes that the tonal units are morphemes of different kinds and those phonetic rules translate the abstract representations into concrete F0 contours. Thus, phonological aspects of intonation can be categorized according to the inventory of the phonological tones, and to the meanings assigned to phonological tones of a specific language. However it is the ToBI (Tones and Break Indices: Beckman & Hirshberg, 1994; Beckman & Ayers 1997) transcription that was designed for this presentation, of the phonological tones, within the AM theory.

ToBI was first designed for Mainstream American English and then expanded into a general framework for the development of prosodic annotation systems of different typological languages (Jun, 2005). ToBI has been applied to a wide variety of languages that vary geographically, typologically and according to their degree of lexical specifications, and to tone languages. For the purpose of the present research, an IH-ToBI was established in order to create a systematic procedure for transcribing data for Israeli Hebrew (Green, 2009a, 2010; Green & Tobin, 2008a).

3.2.2 The inventory of the IH prosodic features of Intonation (IH-ToBI)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The starting point for the analysis of the prosodic pitch contour i.e. intonation, is the notion of an intonation unit. This unit can be defined by its phonetic-phonological characteristics: (a) there is a "unity of pattern" within the intonation unit i.e. the intonation unit has a distinct intonation pitch pattern, and (b) the intonation unit is delimited by a boundary tone.  In IH-ToBI, the intonational structure of the utterance is represented by a “Tone Tier” and three types of tonal events can be identified: (a) pitch accents, the event that associates with stressed syllables and two types of phrasal tones; (b) phrase accents and (c) boundary tones.  Therefore, on the “Tone Tier” the perceived pitch contour is transcribed in terms of: (1) Pitch Accents (PAs) (2) Phrase Accents, and (3) Edge Tones (the last phrase accent and the final boundary tone).

In IH-ToBI every intonational phrase contains at least one Pitch Accent (PA). PAs are localized pitch events that associate with the stressed syllable but in contrast to stress (which is lexically determined), in the tone domain it is not expected that every stressed syllable will be accented. In the AM theory, PAs are perceptually significant changes in F0 aligned with particular words in an utterance, and give them prominence. IH-ToBI identified five PAs: two mono-tonal: high (H) and low (L) tonal patterns: H* and L*, and three bi-tonal: L+H*, H*+L and L*+H. As in other language's descriptions, the H and L tones are described as high or low relative to each speaker's pitch range. The H*- a high pitch accent starting from the speaker’s middle range and realized as a F0 peak preceded by a small rise is by far the most frequently used pitch accent in IH.

Phrase accents and Boundary tones: IH-ToBI identifies two levels of phrasing: (a) the intermediate phrase and (b) the intonation unit. Each intonation unit contains at least one intermediate phrase. The edge tones for these phrases determine the contour from the last tone of the last pitch accent until the end of the phrase. There are two types of phrase accents in IH: (a) ‘Hp’ and (b) ‘Lp’. Hp has roughly the same F0 value as the peak corresponding to the most recent H tone, which creates a plateau at the end of the phrase.  Lp can either be a F0 minimum low in the range, or be down-stepped in relation to a previous tone.

Concerning the boundary tones, IH-ToBI identified three types: (a) an initial boundary tone ‘%’, (b) a high boundary tone ‘H%’, and (c) a low boundary tone ‘L%’. The two final boundary tones combine with the phrase accents in four different combinations i.e. the last intermediate phrase accent (Hp or Lp) combines with the intonational boundary tones to yield the configuration of LpL%, LpH%, HpL% or HpH%. These boundaries appear to have specific pragmatic functions. By analyzing the distribution of these configurations appearing in the spontaneous speech and the reading aloud corpus of our data, it was evident that LpL% is the most frequently used boundary tone in IH and the L-boundary tone signals finality. The absence of finality i.e. signaling a continuation, is marked by a high (H) boundary tone or high phrase accent (Hp) with a L-boundary tone i.e., LpH%, HpH%, HpL%.

To conclude, the richness within the prosodic features (five pitch accents, two phrase accents three boundary tones and all their possible combinations) serve as the basis for the comparing and contrasting of the speech prosody of the ASD-HF subjects with their peers - the WDD controls.

Regarding our investigation of the realization of pitch accents in the speech of children with ASD-HF, our research concentrated on three variables to be analyzed: (1) frequency of high PAs occurrences, (2) distribution of the different IH PAs, and (3) PAs per word (PAs/W), followed by a case investigation of one subject and his matched peer, in order to explore the differences found at the lexical word level.

We found that the children with ASD-HF produced more high PAs than the control group of WDD children in both the reading aloud and spontaneous speech elicitation tasks, without statistical significance, but with high standard deviation within the research group.  This high standard deviation shows that the variability within the ASD-HF research group is much greater than that within the WDD control group. In a comparison of peers within the groups, in seven of the nine matched peers, the ASD-HF participant showed a greater use of high PAs in the spontaneous speech task and in six matched peers, the ASD-HF participant shows a greater use of high PAs in the reading aloud task.

In the WDD control group only two participants demonstrate above 80% use of high PAs, while in the ASD-HF research group four participants produced above 80% use of high PAs.  No participants in the ASD-HF research group produced less than 70% high PAs while in the WDD control group there are three participants with less than 70% high PAs. The differences arise when comparing the research group and the controls as a group in contrast with a comparison of peers – as a “group of case-studies”. These intergroup differences lead to the conclusion that the characteristic of heterogeneity (e.g. Beglinger, 2001; Firth, 2004; Happe’ & Frith, 1996a) within the ASD classification has methodological implications for research procedure in general and in the present research in particular: i.e. it was the aggregation of peer comparisons that motivated the exploration of the prosodic behavioral features in the group of subjects diagnosed with ASD-HF.

Concerning the PA prosodic feature, the most prominent results deal with PAs/W and the placement of PAs. In a peer-case-investigation it was evident that the ASD-HF subject produced almost twice as often, more PAs in function words than his mach-peer did, and in particular more than one PA per word, while his WDD peer hardly ever adds more than one PA in a word (15.69% of the words in the ASD-HF speech sample and 1.96% of the words in his WDD peer speech sample). These results are illustrated in the following example This example is a sentence from the reading aloud elicitation task: yom "exad yac" a "orit lesaxek ba-xacer lefet" a ra'ata kadur Qatan umuzar munax ba-gina. (Translation: One day Orit went to play in the yard and suddenly saw a small, strange ball in the garden). In this example, the ASD-HF subject (1a below) produced the sentence with three intonation units.  Every word has a PA. Function words (FW) are emphasized with a PA as well as content words (CW). The words /baxacer/ (yard) and /qatan/ (small) have two PAs each. In contrast, the matched pair (1b below) produced the same sentence with only two intonation units. Not every word has a PA and none of the words has more than one PA.

 (1a) 1-ADR-ASD
 IU-1:    /yom 'exad       yac'a       'Orit      lesaxeq /
 Gloss:   day one          to go out   (name)     to play
          FW               CW          CW         CW
 IU-2:    / ba- xacer/
 Gloss:     in+yard
            FW+CW
 IU-3:    /lefet'a     ra`ata     Kadur qa-Tan umuzar       munax        ba- gina/
 Gloss:   suddenly     to see     ball small and+strange placed          in+garden
           FW          CW         CW CW        FW+CW        CW           FW+CW

 (1b) 11-AVY-WDD
 IU-1: /yom 'exad yac'a 'Orit lesaxek ba-xacer/
 IU-2: /lefet'a ra`ata kadur qatan umuzar munax ba-gina/

As was previously found (e.g. Baltaxe, 1984; Balataxe & Guthrie, 1987; Fosnot & Jun, 1999; MacCaleb & Prizant, 1985), and extending over in the current research, it can be concluded that within the ASD individuals that exhibited atypicality in prosody ‘accents’ are likely to be affected.

Regarding the investigation of boundary tones and phrase accents, a variation in the distribution of edge contour patterns arises when comparing the edge contours of the matched peers within the groups. The research group subjects may be divided into two sub- groups:

a. ASD-HF subjects that produced a full repertoire of edge contour patterns, similar to the control group (4 subjects). Figure 2 is an example of the full repertoire prosodic behavior by the ASD-HF subject compared with his matched peer in the spontaneous speech elicitation task.
b. Of the nine matched peers, in the spontaneous elicitation task five of the subjects produced a varied limited repeatedly used repertoire of the edge contour patterns.  Figure 3 presents the distribution of the edge contour patterns of two subjects in the reading aloud elicitation task and Figure 4 presents the results of five subjects in the spontaneous speech elicitation task.

Fig. 2. Full repertoire of edge contour patterns. This figure shows the comparison between the edge contour patterns of 7-ETR-ASD, age: (12:5) and his matched peer LIS-WDD, age: (12:3) in the spontaneous speech task. The ASD-HF subject uses the same patterns as his peer and has the full repertoire of edge contour patterns (Green, 2010:106)

Fig. 3. Limited repeatedly used repertoire in the reading aloud elicitation task (Green, 2010:106)

Fig. 4. Limited repeatedly used repertoire in the Spontaneous speech ellicitation task (Green, 2010:107)

In conclusion we will emphasize certain aspects that were manifested in the present study.  The starting point in our study was the need to characterize the prosodic features of children diagnosed with ASD-HF who mainstreamed in regular schools. The research established methodology, which allows the analysis of more than one feature of prosody simultaneously and described, compared and contrasted the phonetic realization of the fundamental frequency and the prosodic features of intonation in the language of 10 children with ASD-HF and 10 children WDD. By using recently developed speech technology tools, we performed an extensive investigation of the prosody of children with ASD-HF between the ages of 9-13 years. The speech sample analysis yielded quantitative results, of group comparison, peer comparison and of subjects within the ASD-HF group.

The peer comparison highlights the greater variations within the ASD-HF subjects, as compared with their peers and between themselves. From this study we can conclude that not all ASD-HF subjects present an a-typicality in each of the different prosodic features examined, but no subject performed in the same way as his WDD peer.

It was found that ASD-HF subjects produce more high PAs and less low PAs. If the variations in intonation are a result of differences in the kinds of PAs and transitions between the prominent components, then when the prominence in the ASD-HF subjects exists in a more frequent single high PA and there are consequently fewer transitions, a monotonous accent is created. The ASD-HF present repetitive behavior expressed over the use of pitch accents within a word - a repetitiveness that did not observed in the control group.

One of the most significant founding is concern with the use of edge tone i.e., the tonal events at the edge of prosodic domains. The ASD-HF subjects primarily use three different edge tone patterns, although they do make a very limited use of all the other patterns. Thus, the problem is not the absence of patterns due to lack of competence to produce them, rather it is the nature of the behavior that the ASD-HF exhibited. Although the ASD-HF subjects are capable of producing a wide range of prosodic patterns, they concentrate on a limited repertoire of the most basic prosodic patterns. Both the monotonous accent and the repetitiveness of edge tones create a stiff sounding prosody in subjects within the ASD-HF group.

Our claim based on all the data collected and results from our research indicates that the restricted repetitive behavior of the ASD-HF subjects, appears in a parallel way across the board in the extralinguistic, paralinguistic (prosody) and linguistic (lexical choice) domains.  Then, Turner's distinction between higher and lower level behavioral categories may only reflect the observable symptoms of ASD behavior rather than their fundamental motivation.  We suggest that the concept of limited and repetitive behavior found on all levels of extralinguistic, paralinguistic and linguistic behaviors in a parallel way among different populations with ASD should play a more central role in research to help us better understand ASD.

4.References
=============

.. [1]_ American Psychiatric Association. (1994). Diagnostic and statistical manual of mental disorders (4th ed.). Washington, DC: American Psychiatric Association.
.. [2]_ Baltaxe, C. A., (1984). Use of contrastive stress in normal, aphasic, and autistic children.  Journal of speech and Hearing Research, 27, 97-105.
.. [3]_ Baltaxe, C. A., & Guthrie, D. (1987). The use of primary sentence stress by normal, aphasic and autistic children. Journal of Autism and Developmental Disorders, 17, 255-271.
.. [4]_ Bartak L, Rutter M. (1996). Differences between mentally retarded and normally intelligent autistic children. Journal of Autism Childhood Schizophrenia, 6, 109-120.
.. [5]_ Beglinger, L. G., & Smith, T. H. (2001). A review of subtyping in autism and proposed dimensional classification model. Journal of Autism and Developmental Disorders, 31, 411-422.
.. [6]_ Bishop, S., Richler, J., & Lord, C. (2006). Association between restricted and repetitive behavior and nonverbal IQ in children with autism spectrum disorders. Child Neuropsychology, 12, 247-267.
.. [6]_ Bodfish, J. W., Symons, F. J., Parker D. E., & Lewis, M. H. (2000). Varieties of repetitive behavior in autism: comparisons to mental retardation. Journal of Autism and Developmental Disorders, 30, 237-243.
.. [7]_ Bonadonna, P. (1981). Effects of a vestibular stimulation program on stereotypic rocking behaviour. The American Journal of Occupational Therapy, 35(12), 775-781.
.. [8]_ Boersma, P. & Weenink, D. (2009). Praat: doing phonetics by computer (Version 5.1.05).  Retrieved May 1, 2009, from http://www.praat.org
.. [9]_ Campbell M, Locascio J, Choroco B, et al. (1990). Stereotypies and tardive dyskinesia: Abnormal movements in autistic children. Psychopharmacol Bull, 26, 260-266.
.. [10]_ Cuccaro, M. L., Shao, Y., Grubber, J., Slifer, M., Wolpert, C. M., Donnelly, S. L., et al. (2003).  Factor analysis of restricted and repetitive behaviors in autism using the Autism Diagnostic Interview-R. Child Psychiatry and Human Development, 34, 3–17.
.. [11]_ Diver, W. (1979). Phonology as human behaviour. In D. Aaronson & P. Reiber (Eds.), Psycholinguistic research: Implications and applications (pp. 161-186). Hillside, NJ: Lawrence Erlbaum.
.. [11]_ Diver, W. (1998). The theory. In E. Contini-Morava & B. S. Goldberg (Eds.), Meaning as explanation: Advances in linguistic sign theory (pp. 43-114). Berlin: Mouton de Gruyter.
.. [12]_ Dollaghan, Ch. A., Campbell, T. F, Paradise, J. L., Feldman, H. M., Janosky, J. E., Pitcairn, D.  N., & Kurs-Lasky, M. (1999). Maternal education and measures of early speech and language. Journal of Speech, Language, and Hearing Research, 42, 1432-1443.
.. [13]_ Durand, V. M., & Carr, E. G. (1987). Social Influences on ‘‘selfstimulatory’’ behavior: Analysis and treatment applications. Journal of Applied Behavior Analysis, 20(2), 119-132.  Esbensen, A. J., Seltzer, M. M., Kristen S., Lam, L., & Bodfish, J. W. (2009). Age-related differences in restricted repetitive behaviors in autism spectrum disorders. Journal of Autism and Developmental Disorders, 39, 57-66.
.. [13]_ Fecteau, S., Mottron, L., Berthiaume, C., & Burack, J. A. (2003). Developmental changes of autistic symptoms. Autism, 7, 255-268.
.. [14]_ Fosnot, S., & Jun, S. A. (1999). Prosodic characteristics in children with stuttering or autism during reading and imitation. Proceedings of 14th International Congress of Phonetic Sciences (pp.1925-1928).
.. [15]_ Frith, U. (2004). Emanuel Miller lecture: confusions and controversies about Asperger syndrome, Journal of Child Psychology and Psychiatry, 45, 672-686.
.. [16]_ Fujisaki, H. (1997). Prosody, models, and spontaneous speech, In Y. Sagisaka., N. Campbell & N. Higuchi (Eds.), Computing prosody: Computational models for processing spontaneous speech (pp. 27-40). New York: Springer.
.. [17]_ Gabriels, R. L., Cuccaro, M. L., Hill, D. E., Ivers, B. J., & Goldson, E. (2005). Repetitive behaviors in autism: Relationships with associated clinical features. Research in Developmental Disabilities, 26, 169–181.
.. [18]_ Green, H. (2005). The contour of the prosodic intonation in the spoken language of children with high functioning autism according to the theory of phonology as human behavior. M.A.  Thesis, Ben-Gurion University of the Negev, Be'er-Sheva: Israel.
.. [19]_ Green, H., & Tobin, Y. (2008a). A phonetic analysis of the prosody of Hebrew-speaking children with high functioning autism. Proceedings of the 18th International Congress of Linguists. Seoul, Korea. (CD ROM).
.. [20]_ Green, H., & Tobin, Y. (2008b). Intonation in Hebrew speaking children with high- functioning autism: A case study. In A. B. Plinio., S. Madureira & C. Reis (Eds.), Proceedings of the fourth conference on speech prosody SP2008 (pp.237-240). Campinas, Brazil.
.. [21]_ Green, H. (2009a). Intonation in Hebrew-speaking children with high functioning autism.  Asian-Pacific Journal of Speech, Language and Hearing, 12, 187-198.
.. [22]_ Green, H., & Tobin, Y. (2009b). Prosodic analysis is difficult…but worth it: A study in high functioning autism. International Journal of Speech-Language Pathology, 11, 308-316.
.. [23]_ Green, H., & Tobin, Y. (2009c). Phonology as human behavior: A prosodic study of high functioning autism. In: Victoria Marrero Idaira Pineda (eds.). La Lingüística ante el reto de la aplicación clínica/Linguistics: The Challenge of Clinical Application: Actas del II Congreso Internacional de Lingüística Clínica/Proceedings of the 2nd International Clinical Linguistics Conference, Madrid November 11-13, 2009. Madrid: Universidad de Nacional Educación a Distancia and Euphonia Ediciones, pp. 378-382.
.. [24]_ Green, H. (2010). Prosodic features in the spoken language of children with autism spectrum disorders high-functioning (ASD-HF) according to the theory of phonology as human behavior. PhD. Thesis, Ben-Gurion University of the Negev, Be'er-Sheva: Israel.
.. [25]_ Happé, F. G., & Frith, U. (1996). The neuropsychology of autism. Brain, 119, 1377-1400.
.. [26]_ Jun, S. A. (Ed.) (2005). Prosodic typology: The phonology of intonation and phrasing.  Oxford: Oxford University Press.
.. [27]_ Kanner, L. (1943). Autistic disturbances of affective contact. Nervous Child, 2, 217-250.
.. [28]_ Kanner, L. (1971). Follow-up study of eleven autistic children originally reported in 1943.  Journal of Autism and Childhood Schizophrenia, 1, 119-145.
.. [30]_ Ladd, R. R. (1996). Intonational phonology. Cambridge: Cambridge University Press.
.. [31]_ Lam, K. S. L., & Aman, M. G. (2007). The Repetitive Behavior Scale-Revised: Independent validation in individuals with autism spectrum disorders. Journal of Autism and Developmental Disorders, 37, 855–866.
.. [32]_ Lecavalier, L., Aman, M. G., Scahill, L., McDougle, C. J., McCracken, J. T., Vitiello, B., et al.  (2006). Validity of the autism diagnostic interview-revised. American Journal of Mental Retardation, 111, 199-215.
.. [33]_ Lewis, M. H., & Bodfish, J. W. (1998). Repetitive behavior disorders in autism. Mental Retardation and Developmental Disabilities Research Reviews, 4, 80-89.
.. [34]_ McCaleb, P., & Prizant, B. M. (1985). Encoding of new versus old information by autistic children. Journal of Speech and Hearing Disorders, 50, 230-240.
.. [35]_ McCann, J. & Peppé, S. (2003). Prosody in autism spectrum disorders: a critical review.  International Journal of Language and Communication Disorders, 38, 325-350.
.. [36]_ Militerni, R., Bravaccio, C., Falco, C., Fico, C., & Palermo, M. T. (2002). Repetitive behaviors in autistic disorder. European Child & Adolescent Psychiatry, 11, 210-218.
.. [37]_ Paul, R., Shriberg, L. D., McSweeny, J. L, Cicchetti, D., Klin, A., & Volkmar, F. (2005). Brief report: Relations between prosodic performance and communication and socialization ratings in high functioning speakers with autism spectrum disorders.  Journal of Autism and Developmental Disorders, 35, 861-869.
.. [38]_ Perkins, M. R., Dobbinson, S., Boucher, J., Bol, S., & Bloom, P. (2006). Lexical knowledge and lexical use in autism. Journal of Autism and Developmental Disorders, 36, 795-805.
.. [39]_ Pierrehumbert, J. B. (1980). The phonology and phonetics of English intonation. Unpublished PhD dissertation, MIT. Reproduced by Indiana University Linguistics Club, Bloomington, IN.Pierrehumbert, J. & Hirschberg, J. (1990). The meaning of intonation in the interpretation of discourse. In P. Cohen., J. Morgan & M. Pollack (Eds.), Intentions in communication (pp. 271-311). Cambridge: MIT Press.
.. [40]_ Piven, J., Harper, J., Palmer, P., & Arndt, S. (1996). Course of behavioral change in autism: A retrospective study of high-IQ adolescents and adults. Journal of the American Academy of Child and Adolescent Psychiatry, 35, 523–529.
.. [41]_ Prior, M., MacMillan, M. (1973). Maintenance of sameness in children with Kanner’s Syndrome. Journal of Autism and Childhood Schizophrenia, 3, 154-167.
.. [42]_ Richler, J., Bishop, S. L., Kleinke, J. R., & Lord, C. (2007). Restricted and repetitive behaviors in young children with autism spectrum disorders. Journal of Autism and Developmental Disorders, 37, 73-85.
.. [43]_ Rutter, M. (1996). Autism research: Prospects and progress. Journal of Autism and Developmental Disorders, 26, 257-275.
.. [44]_ Simmons, J. Q., & Baltaxe, C. (1975). Language patterns in adolescent autistics. Journal of Autism and Childhood Schizophrenia, 5, 333-351.
.. [45]_ Szatmari, P., Georgiades, S., Bryson, S., Zwaigenbaum, L., Roberts, W., Mahoney, W., et al.  (2006). Investigating the structure of the restricted, repetitive behaviours and interests domain of autism. Journal of Child Psychology and Psychiatry, 47, 582-590.
.. [46]_ Tobin, Y. (1990). Semiotics and linguistics. London: Longman.
.. [47]_ Tobin, Y. (1993). Aspect in the English verb: Process and result in language. London: Longman.
.. [48]_ Tobin, Y. (1994). Invariance, markedness and distinctive feature analysis: A contrastive study of sign systems in English and Hebrew. Amsterdam and Philadelphia: John Benjamins.
.. [49]_ Tobin, Y. (1997). Phonology as human behaviour: Theoretical implications and clinical applications.  Durham, NC: Duke University Press.
.. [50]_ Tobin, Y. (2009). Phonology as human behaviour: Applying theory to the clinic. Asian-Pacific Journal of Speech, Language and Hearing, 12, 81-100.
.. [51]_ Turner, M. (1997). Towards an executive dysfunction account for repetitive behaviour in autism. In J. Russell (Ed.), Autism as an executive disorder (pp. 57–100). New York: Oxford University Press.
.. [52]_ Turner, M. (1999). Annotation repetitive behaviors in autism: A review of psychological research. Journal of Child Psychology and Psychiatry and Allied Disciplines, 40, 839-849.
.. [53]_ Varni, J. W., Lovaas, O. I., Koegel, R. L., & Everett, N. L. (1979). An analysis of observational learning in autistic and normal children. Journal of Abnormal Child Psychology, 7, 31- 43.
.. [54]_ Wing, L., & Gould, J. (1979). Severe impairments of social interaction and associated abnormalities in children: epidemiology and classification, Journal of Autism and Developmental Disorders, 9, 11-29.
