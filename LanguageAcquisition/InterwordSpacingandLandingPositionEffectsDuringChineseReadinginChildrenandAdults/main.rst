############################################################################################
Interword Spacing and Landing Position Effects During Chinese Reading in Children and Adults
############################################################################################

- Chuanli Zang 
- Feifei Liang 
- Xuejun Bai
- Guoli Yan
- Simon P. Liversedge

Abstract
=============

本研究はスペースの空いている本を読んでいるときと,スペースの区切りのない中国語のテキストを読んでいるときの子供と大人の目の動きの行動を調査した.
その結果,単語間のスペースは子供も大人も初回のリーディングタイムを減少させ,再固定確率が単語識別と促進された単語の間のスペースを示している.
単語の
The present study examined children and adults’ eye movement behavior when reading word spaced and unspaced Chinese text. 
The results showed that interword spacing reduced children and adults’ first pass reading times and refixation probabilities indicating spaces between words facilitated word identification. 
Word spacing effects occurred to a similar degree for both children and adults, though there were differential landing position effects for single and multiple fixation situations in both groups; clear preferred viewing location effects occurred for single fixations, whereas landing positions were closer to word beginnings, and further into the word for adults than children for multiple fixation situations. 
Furthermore, adults targeted refixations contingent on initial landing positions to a greater degree than did children. 
Overall, the results indicate that some aspects of children’s eye movements during reading show similar levels of maturity to adults, while others do not.
