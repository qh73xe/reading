###########################################################################################
Motherese in Interaction: At the Cross-Road of Emotion and Cognition? (A Systematic Review)
###########################################################################################

.. contents:: 目次
   :depth: 4

- Catherine Saint-Georges [#f1]_ , [#f2]_ 
- Mohamed Chetouani [#f2]_ 
- Raquel Cassel [#f1]_ , [#f3]_ 
- Fabio Apicella [#f4]_ 
- Ammar Mahdhaoui [#f2]_ 
- Filippo Muratori [#f4]_ 
- Marie-Christine Laznik [#f5]_
- David Cohen [#f1]_ , [#f2]_

.. note::

   - Citation: Saint-Georges C, Chetouani M, Cassel R, Apicella F, Mahdhaoui A, et al. (2013) Motherese in Interaction: At the Cross-Road of Emotion and Cognition? (A Systematic Review). PLoS ONE 8(10): e78103. doi:10.1371/journal.pone.0078103
   - Editor: Atsushi Senju, Birkbeck, University of London, United Kingdom
   - Received July 19, 2013; Accepted September 6, 2013; Published October 18, 2013 Copyright: © 2013 Saint-Georges et al. This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.
   - Funding: The authors have no support or funding to report.
   - Competing interests: The authors have declared that no competing interests exist.
       - E-mail: david.cohen@psl.aphp.fr


Abstract (要約)
================

Various aspects of motherese also known as infant-directed speech (IDS) have been studied for many years. 
As it is a widespread phenomenon, it is suspected to play some important roles in infant development. 
Therefore, our purpose was to provide an update of the evidence accumulated by reviewing all of the empirical or experimental studies that have been published since 1966 on IDS driving factors and impacts. 
Two databases were screened and 144 relevant studies were retained. 
General linguistic and prosodic characteristics of IDS were found in a variety of languages, and IDS was not restricted to mothers. 
IDS varied with factors associated with the caregiver (e.g., cultural, psychological and physiological) and the infant (e.g., reactivity and interactive feedback). 
IDS promoted infants’ affect, attention and language learning. 
Cognitive aspects of IDS have been widely studied whereas affective ones still need to be developed. 
However, during interactions, the following two observations were notable: 

- (1) IDS prosody reflects emotional charges and meets infants’ preferences, 
- and (2) mother-infant contingency and synchrony are crucial for IDS production and prolongation. 

Thus, IDS is part of an interactive loop that may play an important role in infants’ cognitive and social development.

.. note:: 翻訳

   対乳児音声 ( Infant-Directed Speech : IDS ) としても知られるマザリースの色々な側面は長い間研究されてきた.
   これは広く起きる現象であるため, 乳児の発達において重要な役割を果たすのではないかと思われてきている.
   したがって, 我々の目的は 1966年からの IDS の起きる要因と影響について公開されてきた経験的, 実験的研究のすべてをレビューすることによって, 蓄積された証拠の更新を行うことである.
   2つのデータベースをスクリーニングした結果, 144個の関連研究が収集された.
   一般に, IDS の 言語学的, もしくは 韻律的特徴 が 様々な言語で 母親に限らず 発見されている.
   IDS は 保護者 ( 例えば、文化的心理的、生理的) と 乳児 (例えば、反応性および対話型のフィードバック) に関係する要因によって変化する.
   IDS は 乳児の注目や言語獲得を促進に影響を与える.
   IDS の認知的な側面は情動的なものの発達にも必要とされるのか否かを広く研究している.
   しかし, 相互作用の間に, 以下の2つの観測が注目すべきものであった.
   
   1. IDSの韻律は感情的なものを反映しており、乳幼児の好みを満たしている
   2. 母-子 の偶発性と同期性は IDS 発話と延長にとって重要なものである

   そのため, IDSは 乳児の認知的,社会的な発達に重要な役割をは立つ 相互作用的なループの一部である.

Introduction (導入)
======================

Motherese, also known as infant-directed speech (IDS) or “baby-talk”, refers to the spontaneous way in which mothers, fathers, and caregivers speak with infants and young children.  
In a review of the various terms used to denote young children’s language environments, Saxton suggested the preferential use of “infant or child-directed speech” [1]_ . 
In 1964, a linguist [2]_ defined “baby-talk” as “a linguistic subsystem regarded by a speech community as being primarily appropriate for talking to young children”. 
He reported that “baby talk” was a well-known, special form of speech that occurred in a number of languages and included the following 3 characteristics: 

- (1) intonational and paralinguistic phenomena (e.g., a higher overall pitch), 
- (2) words and constructions derived from the normal language (e.g., the use of third person constructions to replace first and second person constructions), 
- (3) a set of lexical items that are specific for baby talk. 

He provided a precise, documented study of IDS across several different languages. 
Since then, infant-directed speech has been studied extensively across a number of interactive situations and contexts, especially by researchers interested in understanding language acquisition. 
A recent review of “babytalk” literature focused on phonological, lexical and syntactic aspects of the input provided to infants from the perspective of language acquisition and comprehension [3]_ . 
Although Snow, in a review of the early literature on motherese [4]_ , 
claimed that “language acquisition is the result of a process of interaction between mother and child, which begins early in infancy, 
to which the child makes as important a contribution as the mother, 
and which is crucial to cognitive and emotional development as well as language acquisition”, 
few experimental findings have sustained this assertion. 
Recent progresses in cognitive science and in interactional perspective suggest, however, that infant cognitive development is linked with social interaction (e.g., Kuhl et al., 2003). 
Motherese could be a crossroad for such a linkage. 
Here, we aim to review the available evidence relevant to motherese from an interactional perspective, with a specific focus on children younger than 2 years of age. 
In contrast with Soderstrom’s review (2007), we focus more preferentially on motherese’s prosodic and affective aspects to determine the factors, 
including interactive ones, associated with its production and variations, its known effects on infants and its suspected functions aside from language acquisition.

.. note::

   IDS や "ベイビー・トーク" としても知られているマザリーズとは, 母親, 父親, その他介護者が乳幼児と自然に話した際の話し方を指す.
   乳児の言語環境を指すために使用される様々な用語のレビューにおいて, Saxton は " 乳児-児童発話 " の優先的な使用を示唆している.
   1964 年に, ある言語学者が "ベイビー・トーク" を "幼い子どもに話しかけるための主要で適切であると発話コミュニティによってみなされる言語的なサブシステム" であると定義した.
   彼は "ベイビー・トーク" は一般に広く知られている, 多くの言語で発生する, 以下の3つの特徴が含まれている特別な発話の形式であると報告した.
   
   1. イントネーションやパラ言語情報 (例えば, ピッチが高い)
   2. 通常の言語から派生した単語や構造 (例えば, 第一, 第二人称と交換される第三人称の使用)
   3. ベイビー・トーク のための特別な語彙のセット
  
   彼は、複数の異なる言語を横断して、IDSに関する正確で文書化された研究を提供した。
   その時以来、IDS は、特に言語獲得の理解に興味を持っている研究者によって、多くの対話の状況および文脈を横切って広範囲に研究された。
   最近の "ベイビー・トーク" に関する文献は言語獲得と理解の側面から乳児に提供される、入力音声の 音韻的、 韻律的、文法的側面に注目している。
   Snow がマザリーズに関する初期の文献のレビューの中で述べられている "言語獲得とは乳幼児期の初期に始まる、母親と子供のインタラクション過程の結果であり、
   それに大して乳児は母親と同等の貢献を果たし、言語獲得と同様に認知および感情的な発達に不可欠である" という主張はいくつかの実験の結果支持されている。
   しかし、認知科学とインタラクション的な知見の最近の発展では、乳児の認知的発達は社会的インタラクションと関連していることを示唆している（例えば、kuhl et al., 2003）。
   マザリーズはこのような関連のための交差点であるかもしれない。
   そこで、我々は２歳未満の乳児に焦点を絞り、インタラクションの視点からマザリーズに関連する利用可能な証拠を整理することを目的とする。
   Soderstrom のレビュー (2007) とは対照的に, 我々は要因を特定しやすくするために、インタラクティブなものも含むマザリーズの韻律的と感情的側面について
   その生成および変形に関連した乳幼児への既存の効果と言語獲得側からでは疑いのある機能に関してより優先的に焦点をあわせる。

Methods (方法)
===================

We searched the PubMed and PsycInfo databases from January 1966 to March 2011 using the following criteria: 
journal article or book chapter with ‘‘motherese’’ or ‘‘infant-directed speech’’ within the title or abstract, published in the English language and limited to human subjects. 
A diagram summarizing the literature search process is provided in :num:`Figure #diagram-flow` . 
We found 90 papers with PubMed and 134 with PsycInfo, of which 59 were shared across the databases, for a total of 165 papers. 
We excluded 50 papers because 11 were reviews or essays and 39 were experimental studies that did not aim to improve knowledge on IDS as they addressed other aims (see details in Annex S1). 
We found an additional 29 references by screening the reference lists of the 115 papers, leading to a total of 144 relevant papers.

.. note::

   我々は PubMed と PsycInfo ベータベースを 1966 年 １月 から 2011 年 3月まで以下の基準で検索を行った。
   
   - "マザリーズ" もしくは "infant-directed speech" という単語をタイトルもしくはアブストラクトに含む ジャーナルあるいは本の章
   - 英語で出版されているもの
   - 人間を対象にしているもの

   文献検索の過程は :num:`Figure #diagram-flow` に示す。↩

.. _diagram-flow:

.. figure:: fig/fig1.png
   
   Diagram flow of the literature search.

   - doi: 10.1371/journal.pone.0078103.g001

.. note::

   我々は 90 本の論文を PubMed から見つけ、 PsyInfo からは 134 本見つけた。
   そのうち 59 本はデータベース上に重複があったため、最終的には 165 本である。
   我々はそのうち 50 本の論文を除外した。
   これらのうち 11 本はレビューやエッセイであり、39 本は IDS に関する知見を向上する以外の目的の実験的研究であった（詳細は 付録 S1 を参照）。
   我々は 115 本の論文の引用を再度スクリーニングし、 関連する論文 29 本を追加した。
   合計 144 の関連する論文を発見した。

Results (結果)
===================

1: General comments (総括)
----------------------------

Table 1 lists the relevant studies and the number of subjects included according to each domain of interest. 
The following observations are evident: 

- (1) certain points are well documented (e.g., IDS’s effect on language acquisition), whereas others have received less support (e.g., IDS production according to gender and the course of infants’ preference for IDS); 
- (2) the sample sizes between studies range from 1 to 276 with 1/3 of the studies having N≤15, 1/3 having 15<N<40, and 1/3 having N≥40; 
- and (3) methodologies vary greatly between studies with regard to design and sample characteristics (e.g., the type of locator and infants’ ages). 

The results are presented in several sections. 
Concerning IDS production, we will first review its general characteristics and, then, its variations according to maternal language, 
infants’ age, gender, vocalizations, abilities and reactivities, and parental individual differences. 
For IDS’s effects on infants, we listed the following 4 main functions of IDS: 
communicating affect, facilitating social interaction through infants’ preferences, engaging and maintaining infants’ attention, and facilitating language acquisition. 
The discussion incorporates selected articles dealing with theoretical considerations and those that included the boundaries of the concept of motherese.

.. note::

   表１は関連研究とそれぞれの興味領域に含まれる被験者数のリストである．
   以下の観察結果が証明されている．
   
   1. 他の領域があまりサポートされていないのに対し，特定のポイントに関してはよく文章化されている．
       - 例えば，性別や乳児の好みの方向に応じた IDS の発声に対して，言語獲得上の IDS の効果については研究がされている．
   2. 研究間のサンプルサイズの範囲は 1 人から 276 人まで
       - 三分の一の研究は15名以下，三分の一の研究は 15人 より大きく 40人より小さい，残りの三分の一は40名以上
   3. デザインとサンプルの特性に応じて方法論は研究間で大きく異る
       - 例えば，出身地や乳児の年齢の種類など

   結果は複数の章で示している．
   IDS の発声に関しては，我々はその一般的な特徴をレビューする
   その後，母親の言語に応じて乳児の年齢，性別，発声，能力，反応性及び，親の個人差についてレビューする．
   乳児に対するIDSの影響に関して，我々は以下の４つの機能をリストした．
   
   - コニュニケーションに影響を与える機能
   - 乳児の選好と通して社会的相互作用を促進する機能
   - 乳児の注目を引き，維持する機能
   - 言語獲得を容易にする機能

   議論はマザリーズの概念の境界部分を含む研究もいれた論理的な考察を扱う．

.. csv-table:: Table 1. Characteristics of the studies included in the review.
    :header: Author-year , N* subjects , Design of the study , Main objective to explore or assess… (motherese features and variations)
    :widths: 15, 5, 15, 30

    Durkin 1982        , 18           , Cross-sectional observational                       , Functions of use of proper names
    Fernald 1984       , 24           , Paired comparisons IDS/simulated IDS/ADS            , Prosodic features according to infant feed-back
    Fisher 1995        , 20           , Paired comparisons IDS/ADS                          , Prosodic features on new/given words
    Soderstrom 2008    , 2            , Longitudinal case series                            , Prosodic and linguistic features
    Fernald 1991       , 18           , Paired comparisons IDS/ADS                          , Prosodic features on focused words
    Ogle 1993          , 8            , Cross-overIDS/ADS (electrolaryngography)            , Prosodic features (F0 measures)
    Fernald 1989a      , 30           , Paired comparisons IDS/ADS                          , Prosodic features for mothers and fathers across 6 languages
    Niwano 2003b       , 3            , Paired comparisons IDS/ADS                          , Prosodic features for mothers and fathers + infant’s responses
    Shute 1999         , 16           , Paired comparisons IDS/ADS                          , Prosodic features for fathers speaking and reading aloud
    Shute 2001         , 16           , Paired comparisons IDS/ADS                          , Prosodic features for grandmothers speaking and reading aloud
    Nwokah 1987        , 16           , Case-control (Mother/maid)                          , Linguistic and functional features of maids’ IDS
    Katz 1996          , 49           , Paired comparisons with pragmatic categories of IDS , Prosodic contours according to intention
    Stern 1982         , 6            , Case-series                                         , Prosodic contours according to intention & grammar and context
    Papoušek 1991      , 20           , Case-control (Chinese/English)                      , Prosodic contours according to context in different languages
    Slaney 2003        , 12           , Paired comparisons (IDS with various intentions)    , Acoustic measures according to affect (automatic classification)
    Trainor 2000       , 96           , Paired comparisons IDS/ADS with various emotions    , Links between IDS and affective expression
    Inoue 2011         , 24           , Paired comparisons IDS/ADS                          , Wether Mel-frequency cepstral coefficients discriminate IDS from ADS
    Mahdhaoui 2011     , 11           , Paired comparisons IDS/ADS                          , Automatic detection based on prosodic and segmental features
    Cristia 2010       , 55           , Paired comparisons IDS/ADS                          , Enhancement of consonantal categories
    Albin 1996         , 16           , Paired comparisons IDS/ADS                          , Lengthening of word-final syllables
    Swanson 1992       , 15           , Paired comparisons IDS/ADS                          , Vowel duration of content words as opposed to function words
    Swanson 1994       , 22           , Paired comparisons IDS/ADS                          , Vowel duration of function-word in utterance final position
    Englund 2006       , 6            , Longitudinal Paired comparisons IDS/ADS             , Vowels and consonant specification throughout the first semester
    Englund 2005a      , 6            , Longitudinal Paired comparisons IDS/ADS             , Spectral attributes and duration of vowels throughout a semester
    Englund 2005b      , 6            , Longitudinal Paired comparisons IDS/ADS             , Evolution of voice onset time in stops throughout a semester
    Lee 2010           , 10           , Case-control (IDS/ADS)                              , Segmental distribution patterns in English IDS
    Shute 1989         , 8            , Paired comparisons IDS/ADS                          , Pitch Variations in British IDS (compared to American IDS)
    Segal 2009         , 11           , Longitudinal descriptive study                      , Prosodic and lexical features in Hebrew IDS
    Lee 2008           , 10           , Paired comparisons IDS/ADS                          , Segmental distribution patterns in Korean IDS
    Grieser 1988       , 8            , Paired comparisons IDS/ADS                          , Prosodic features in a tone language IDS (Mandarin Chinese)
    Liu 2007           , 16           , Paired comparisons IDS/ADS                          , Exaggeration of lexical tones in Mandarin IDS
    Fais 2010          , 10           , Paired comparisons IDS/ADS                          , Vowel devoicing in Japanese IDS
    Masataka 1992      , 8            , Paired comparisons IDS/ADS                          , Rhythm : repetition and gestual exaggeration in Japanese sign language
    Reilly 1996        , 15           , Longitudinal descriptive study                      , Competition between affect and grammar in American sign language
    Werker 2007        , 30           , Cross-language comparison                           , Differences in distributional properties of vowel phonetic categories
    Kitamura 2003      , 12           , Longitudinal Paired comparisons IDS/ADS             , Pitch and communicative intent according to age
    Stern 1983         , 6            , Longitudinal case-series                            , Prosodic features evolution
    Niwano 2002b       , 50           , Longitudinal case-series                            , Pitch and Prosodic contours according to age
    Liu 2009           , 17           , Longitudinal Paired comparisons IDS/ADS             , Prosodic and phonetic features according to age
    Kajikawa 2004      , 2            , Longitudinal case-series                            , Adult conversational style (Speech overlap) emergence in Japanese IDS
    Amano 2006         , 5            , Longitudinal case-series                            , Changes in F0 according to infant age and language acquisition stage
    Snow 1972          , 12/24/6      , Paired comparisons IDS/CDS                          , Linguistic features according to children age
    Kitamura 2002      , 22           , Longitudinal Paired comparisons IDS/ADS             , Pitch according to infant age and gender in English and Thaï languages
    Braarud 2008       , 32           , Paired comparisons synchrony/dyssynchrony           , IDS quantity according to infant feed-back and synchrony
    Smith 2008         , 18           , Controlled trial (2 experimental groups)            , Pitch variations according to infant feed-back from the pitch
    Shimura 1992       , 8            , Correlation study                                   , Between mother and infant vocalizations (pitch & duration & latency & melody)
    Van Puyvelde 2010  , 15           , Correlation study                                   , Between mother and infant vocalizations (pitch & melody)
    McRoberts 1997     , 1            , Longitudinal case-study                             , Mother & father and infant adjustment of pitch vocalizations during interaction
    Reissland 1999     , 13           , Case-control (premature/term infants)               , Timing and reciprocal vocal responsiveness of mothers and infants
    Niwano 2003a       , 1            , Paired comparisons (mother with twins)              , Pitch and contours variations according to infant reactivity
    Reissland 2002     , 48           , Case-control (age) + Correlation study              , Pitch of IDS surprise exclamation according to infant age/reaction to surprise
    Lederberg 1984     , 15           , Paired comparisons deaf/hearing children            , Adult adjustment in interaction with deaf children
    Fidler 2003        , 36           , Case-control (Down syndrome/other MR)               , Pitch’s mean and variance in parental IDS to Down syndrome/other MR
    Gogate 2000        , 24           , Case-control (5-8;9-17;21-30 months)                , Multimodal IDS according to infants' levels of lexical-mapping development
    Kavanaugh 1982     , 4            , Longitudinal case-series                            , Mother/father linguistic input according to apparition of productive language
    Bohannon 1977      , 20           , Correlation study                                   , MLU of IDS according to child’s feed-back of comprehension
    Bohannon           , 20           , Paired comparisons (manipulating feed-back)         , 
    Bergeson 2006      , 27           , Case-control (cochlear implant/control)             , IDS adjustment (pitch & MLU & rhythm) according to childs' hearing experience
    Kondaurova 2010    , 27           , Longitudinal case-control                           , IDS adjustment according to child’s hearing experience and age
    Ikeda 1999         , 61           , Paired comparisons IDS/ADS                          , Variations according to various life experience (especially having sibling)
    Hoff 2005          , 63           , Prospective study                                   , Variations of linguistic input and teaching practices according to parental socioeconomic status or education & repercussions on child vocabulary-
    Hoff 2005          , 662          , Cross-sectional study                               , 
    Hoff-Ginsberg 1991 , 63           , Cross-sectional study                               , Variations of input according to parental socio-economic status (SES)
    Matsuda 2011       , 65           , Correlation study                                   , Functional RMI of adults listening to IDS according to gender & parental status
    Gordon 2010        , 160          , Prospective study                                   , Oxytocin level according to infant’s age and correlation with parenting
    Bettes 1998        , 36           , Case-control                                        , Maternal behavior (including IDS prosody) according to depression status
    Herrera 2004       , 72           , Case-control                                        , IDS content and touching according to maternal depression status
    Kaplan 2001        , 44           , Correlation study                                   , Variations according to maternal age and depression status
    Wan 2008           , 50           , Case-control                                        , Variations of IDS characteristics according to maternal schizophrenia status
    Nwokah 1999        , 13           , Case-control                                        , IDS amount & structure & content in maids compared with mothers
    Burnham 2002       , 12           , Paired comparisons IDS/ADS/petDS                    , Pitch : affect (intonation + rhythm) and hyperarticulation in IDS versus petDS
    Green 2010         , 25           , Paired comparisons IDS/ADS                          , Lip movements
    Rice 1986          , 2            , Case-series                                         , Description of speech in educational television programs compared with CDS
    Fernald 1989b      , 5            , Paired comparisons IDS/ADS with various intentions  , Adult’s detection of communicative intent according to prosodic contours
    Bryant 2007        , 8            , Paired comparisons IDS/ADS with various intentions  , Adult’s detection of communicative intent according to prosodic contours
    Fernald 1993       , 120          , Paired comparisons IDS/ADS with various intentions  , Communication of affect ( to infants) through prosodic contours
    Papousek 1990      , 32           , Paired comparisons approval/disapproval intent      , Communicating affect (looking response) through prosodic contours
    Santesso 2007      , 39           , Paired comparisons with various affects             , Psycho-physiological (ECG & EEG) responses to IDS with various affects
    Monnot 1999        , 52           , Correlation study                                   , IDS effects on infant’s development level and growth parameters
    Santarcangelo 1988 , 6/4          , Correlation study + paired comparisons IDS/ADS      , Developmentally disabled children’s preference (responsiveness & eye-gaze)
    Werker 1989        , 60           , Paired comparisons IDS/ADS with males/females       , Infant’s preference (looking & facial expression) for male and female IDS
    Schachner 2010     , 20           , Paired comparisons IDS/ADS                          , Subsequent visual infant’s preference for the speaker
    Masataka 1998      , 45           , Paired comparisons IDS/ADS                          , Infant’s preference for infant-directed (versus adult-directed) Sign Language
    Cooper 1993        , 96           , Paired comparisons IDS/ADS                          , 1 month-old infant’s preference for IDS
    Cooper 1990        , 28           , Paired comparisons IDS/ADS                          , Experimental (looking producing IDS) testing of 0-1 month-olds’ preference
    Pegg 1992          , 92           , Paired comparisons IDS/ADS                          , Young infant’s attentional and affective preference for male and female IDS
    Niwano 2002a       , 40           , Paired comparisons with manipulated IDS             , Infant’s preference (through eliciting vocal response)
    Hayashi 2001       , 8            , Longitudinal paired comparisons IDS/ADS             , Developmental change in infant’s preference (according to age)
    Newman 2006        , 90           , Paired comparisons IDS/ADS at 3 ages/2 noise levels , Change in infant’s preference according to developmental age and to noise
    Panneton 2006      , 48           , Paired comparisons with manipulated IDS at 2 ages   ,
    Cooper 1997        , 20/20/23     , 3 Paired comparisons IDS/ADS in various conditions  , Change in infant’s preference according to age and speaker (mother/stranger)
    Hepper 1993        , 30           , Paired comparisons IDS/ADS                          , New-born’s preference for maternal IDS or ADS
    Kitamura 2009      , 24           , 3 Paired comparisons IDS with various contours      , Change in determinants of infant’s preference according to developmental age
    Kaplan 1994        , 45/80        , 2 Paired comparisons IDS with various contours      , Change in determinants of infant’s preference according to developmental age
    Spence 2003        , 42           , 3 Paired comparisons IDS with various intents       , Intent categorization ability according to age (4 months/6 months)
    Johnson 2002       , 210          , Paired comparisons IDS/ADS (prosody or content)     , Adult’s preference for IDS/ADS according to history of head injury
    Cooper 1994        , 12/20/20/16  , 4 Paired comparisons manipulated IDS/ADS            , Do pitch contours determine 1-month-olds’ preference for IDS?
    Fernald 1987       , 20           , Paired comparisons with manipulated IDS             , Do pitch & amplitude or rhythm determine 4-month-olds’ preference for IDS?
    Leibold 2007       , 57           , Paired comparisons with manipulated sounds          , Acoustic determinants of 4-month-olds’ preference for IDS
    Trainor 1998       , 16           , Paired comparisons low or high pitched songs        , Acoustic determinants of infant’s preference for IDS
    Singh 2002         , 36           , Paired comparisons IDS/ADS with various affects     , Does affect (emotional intensity) determine infant’s preference for IDS ?
    McRoberts 2009     , 144/62/24/48 , 4 Paired comparisons with manipulated IDS /ADS      , Does repetition influence infant’s preference for age-inappropriate IDS/ADS?
    Saito 2007         , 20           , Paired comparisons IDS/ADS                          , Does IDS activate brain of neonates (near-infra-red spectroscopy)?
    Kaplan 1996        , 104/78/80    , 3 Paired comparisons IDS/ADS                        , Does IDS (paired with what facial expressions) increase conditioned learning?
    Kaplan 1995        , 77/26        , Paired comparisons IDS/ADS                          , Does IDS engage and maintain infant’s attention?
    Senju 2008         , 20           , Paired comparisons IDS/ADS                          , Does IDS engage infant’s joint attention (eye-tracking) ?
    Nakata 2004        , 43           , Paired comparisons maternal IDS/maternal singing    , Does IDS engage and maintain infant’s attention over singing?
    Kaplan 2002        , 12           , Paired comparisons depressed/non depressed IDS      , Does IDS increase conditioned learning  & according to mother depression?
    Kaplan 1999        , 225          , Controlled trials with IDS varying in quality       , Does IDS increase conditioned learning  & according to mother depressiveness?
    Kaplan 2010a       , 134          , Case-control                                        , Does mother depression duration affect infant’s learning with normal IDS?
    Kaplan 2004        , 40           , Paired comparisons with maternal/female/male IDS    , Does IDS speaker’s gender affect learning by infants of depressed mothers?
    Kaplan 2010b       , 141          , Case-control (2x2 ANOVA)                            , How marital status and mother depression affect learning with male IDS?
    Kaplan 2007        , 39           , Case-control                                        , Does father depression affect infant’s conditioned learning with paternal IDS?
    Kaplan 2009        , 55           , Correlation study                                   , Does maternal sensitivity affect infant’s learning with maternal IDS?
    Karzon 1985        , 192          , Controlled trials: IDS/manipulated IDS/ADS          , Do supra-segmental features of IDS help polysyllabic discrimination?
    Karzon 1989        , 64           , Controlled trials: falling/rising contours          , Does IDS prosody help syllabic discrimination and how?
    Vallabha 2007      ,              , Automatic computed vowels categorization            , Does IDS prosody help categorization of sounds from the native language?
    Trainor 2002       , 96           , Controlled trials                                   , How IDS high pitch / IDS exaggerated contours help vowel discrimination?
    Hirsh-Pasek 1987   , 16/24        , Paired comparisons with manipulated IDS             , Does IDS prosody help to segment speech into clausal units?
    Kemler Nelson 1989 , 32           , Randomized controlled trials with IDS/ADS           , Does IDS/ADS prosody help to segment speech into clausal units?
    Thiessen 2005      , 40           , Controlled trials with IDS/ADS                      , Does IDS prosody help word segmentation?
    D’Odorico 2006     , 18           , Case-control late-talker/typical peers              , Does (prosodic and linguistic) maternal input help language acquisition?
    Curtin 2005        , 24           , Serie of 5 experiments                              , Does lexical stress help language acquisition (speech segmentation)?
    Singh 2008         , 40           , Serie of 4 experiments (controlled trials)          , Does IDS vocal affect help word recognition?
    Colombo 1995       , 27           , Paired comparisons with manipulated sounds          , Does F0 modulation in IDS help words recognition in a noisy ambient?
    Zangl 2007         , 19/17        , Paired comparisons IDS/ADS at 2 ages                , Does IDS/ADS prosody activate brain for familiar and unfamiliar words?
    Song 2010          , 48           , Paired comparisons IDS/manipulated IDS              , Does IDS rhythm/hyper-articulation/pitch amplitude help word recognition?
    Bard 1983          , 94           , 4 Paired comparisons IDS/ADS with adult listeners   , Does IDS help word recognition & according to word contextual predictability?
    Bortfeld 2010      , 16/32/24/80  , 4 paired comparisons IDS words with various stress  , Does emphatic stress in IDS prosody help word recognition ?
    Kirchhoff 2005     , Automatic    , Paired comparisons IDS/ADS words                    , Does IDS prosody help automatic speech recognition ?
    Singh 2009         , 32           , Longitudinal paired comparisons (?) IDS/ADS         , Does IDS prosody help word recognition over the long-term?
    Golinkoff 1995     , 61/79        , Randomized controlled trials IDS/ADS                , Does IDS prosody help adult word recognition in an unfamiliar language?
    Newport 1977       , 12           , Longitudinal prospective correlation study          , Does maternal IDS linguistic properties predict child language acquisition?
    Gleitman 1984      , 6/6          , Same as Newport 1977                                , New analyses on the same data but with 2 age-equated groups
    Scarborough 1986   , 9            , Longitudinal prospective correlation study          , Does maternal IDS linguistic properties predict child language acquisition ?
    Furrow 1979        , 7            , Longitudinal prospective correlation study          , Does maternal IDS linguistic properties predict child language acquisition ?
    Rowe 2008          , 47           , Prospective study                                   , Does input according to parental SES affect child’s vocabulary?
    Hampson 1993       , 45           , Longitudinal prospective study                      , Does maternal IDS linguistic properties predict language acquisition ?
    Waterfall 2010     , 12           , Longitudinal study + computational analysis         , Does IDS linguistic properties help language acquisition?
    Onnis 2008         , 44/29        , Randomized controlled trials Overlap/not            , Does IDS properties (overlapping sentences) help word/grammar acquisition?
    Fernald 2006       , 24           , Paired comparisons with words isolated/not          , Which properties (isolated words/short sentences) help language acquisition?
    Kempe 2005         , 72/168       , Randomized controlled trials Invariance/not         , Does IDS diminutives (final syllable invariance) help word segmentation?
    Kempe 2007         , 486          , Randomized controlled trials Invariance/not         , Does IDS diminutives (final syllable invariance) help word segmentation?
    Kempe 2003         , 46           , Paired comparisons with diminutives/not             , Does IDS diminutives help gender categorization?
    Seva 2007          , 24/22        , Paired comparisons with diminutives/not             , Does IDS diminutives help gender categorization?

2: Motherese characteristics (マザリーズの特徴)
--------------------------------------------------

The general linguistic and paralinguistic characteristics of motherese have been described in several previous works.  
Compared with Adult Directed Speech (ADS), IDS is characterized by shorter [5]_ [6]_  [7]_ , 
linguistically simpler, redundant utterances, which include isolated words and phrases, a large number of questions [7]_ , and the frequent use of proper names [8]_ . 
Regarding rhythm and prosody, longer pauses, a slower tempo, more prosodic repetitions, and a higher mean f0 (fundamental frequency: pitch) and wider f0-range have been reported [5]_ [6]_ [9]_ , 
with these findings supported by electrolaryngographic measures [10]_ . 
Similar patterns of IDS have been observed for fathers and mothers across various languages [11]_ [12]_ [13]_ , except with regard to the wider f0-range, and also for grandmothers interacting with their grandchildren [14]_ . 
In contrast, a maid’s IDS differs significantly from a mother’s IDS with regard to the amount and types of utterances present [15]_ .

.. note::

   マザリーズの一般的な言語学的, パラ言語学的な特徴はいくつかの先行研究において記述されてきた。
   ADS と比較して、IDS は 短く [5]_ [6]_ [7]_ 、言語的にシンプルで、冗長な発話であり、単独の単語やフレーズや多くの質問を含み [7]_ , 固有名詞をよく使用する [8]_ .
   リズムやプロソディーに関しては、より長いポーズで、ゆっくりとしたテンポで、よりプロソディー的な繰り返しが多く、平均 f0 が高く、
   f0 の範囲が広いことが報告されている [5]_ [6]_ , [9]_ .
   これらの発見は電子的な可視化的計測によって支持されている。
   IDS によく出てくるパターンは広い f0 のレンジ以外はいろいろな言語の母親、父親に対して観察されており [11]_ [12]_ [13]_ , 祖母と孫のインタラクションにおいてもみられる [14]_ .
   対照的に 育児経験のない女性の IDS は 母親の IDS とは、表現される発話のタイプと量に関して異なる [15]_ .

Prosodic contours vary according to mothers’ intentions.  
Adults hearing content-filtered speech [16]_  or a language that they do not speak [17]_  were able to use the intonation to identify a mother's intent (e.g., attention bid, approval, and comfort) 
with higher accuracy in IDS than in ADS. 
The prosodic patterns of IDS are more informative than those of ADS, and they provide infants with reliable cues about a speaker’s communicative intent. 
Indeed, f0 contour shape and f0 summary features (i.e., mean, standard deviation, and duration) discriminate the pragmatic categories (e.g., attention, approval, and comfort) from each other [18]_ . 
Mothers of 2 to 6month-old infants use rising contours when seeking to initiate attention and eye contact, 
but they use sinusoidal and bellshaped contours when seeking to maintain eye contact and positive affect with an infant who is already gazing and smiling.  
They also use specific contours for different sentence types, such as rise contours for yes-no questions, fall contours for "wh" questions and commands, and sinusoidal-bell contours for declarative sentences [19]_ . 
Moreover, across different languages, the same types of contours convey the same types of meanings, which include arousing/soothing, turn-opening/ turn-closing, approving/disapproving, and didactic modeling [20]_ . 
Using pitch and spectral-shape measures, a Gaussian mixture-model discriminator designed to track affect in speech classified ADS (neutral affect) and IDS 
with more than 80% accuracy and further classified the affective message of IDS with 70% accuracy [21]_ . 
Indeed, the prosodic features of IDS are related to the widespread expression of emotion towards infants compared with the more inhibited expression of emotion evident in typical adult interactions. 
Few acoustic differences exist between IDS and ADS when expressing love, comfort, fear, and surprise, yet robust differences exist across these emotions [22]_ . 
Furthermore, in contrast with ADS, speech and laughter often co-occur in IDS [23]_ . 
Finally, IDS directed at 6month-old human infants and pet-directed speech (PDS) [24]_  are similar in terms of heightened pitch and greater affect (i.e., intonation and rhythm). 
However, only IDS contains hyperarticulated vowels, which most likely aids in the emergence of language in human infants with both pragmatic and language teaching functions. 
Thus, IDS prosody appears to be crucial for communicating parents’ affect and intentions in a non-verbal way.

.. note::

   韻律の輪郭は母親の興味によって異なる。
   コンテンツフィルタリング発話 [16]_ や 彼らが使用しない言語 [17]_ は ADS よりも IDS において高い精度で
   母親の興味（例えば、意識を向けたり、受諾、快適性など）を特定するためにイントネーションを使用することができる。
   IDS のプロソディー的なパターンは ADS のそれよりも情報が豊富で、これらは乳児に話し手の意思伝達の意図についての手がかりを提供する。
   実際、f0 の輪郭や形状（例えば平均や標準偏差、時速時間）は実際的なカテゴリ（例えば、注意や承認、心地よさ）を互いに判断する。
   2-6 ヶ月児に対するマザリーズはアイコンタクトや注意の開始を探す際に上昇している輪郭を使用するが、かれらは既に見つめ合って笑っている乳児の持つ肯定的な
   効果とアイコンタクトの維持を探索する際に、正弦波とベル状の波を使用する。
   また、彼らは、例えばイエス・ノークエスチョンでは上昇調を示し、”wh” クエスチョンや命令文では下降調を使用し、宣言的判断では正弦ベル調を使用するように、
   異なる文章の型では異なる音調を使用する [19]_ .
   加えて、異なる言語間にわたって、同じ種類の音調は同じ種類の意味を伝える。
   これは, なだめ/喚起、 ターン開閉/ターン開閉、 承認/不承認、 そして 教訓的なモデリングを含む [20]_ .
   ピッチとスペクトル形状の指標を用いて，
   発話における影響を追跡するように設計されたガウス混合分布モデル分類器は80%以上の精度で ADS（ニュートラルな影響）とIDS をクラスタリングし，さらに70%以上の精度で
   IDSに含まれる感情メッセージを分類した [21]_ ．
   確かに，IDS の韻律的特徴は，典型的な大人のインタラクションにおける感情証拠の隠された表現と比較して，乳児に向けた感情の広範な発現に関連している．
   愛情，快適さ，恐怖，驚きを表現する際には，いくつかの音響的な違いはADS,IDS間に存在するが，これらの感情を越えた頑健な差も存在する [22]_ ．
   加えて，ADSとの比較に関して，発話と笑いがIDSにおいてしばしば協調して起きる [23]_ ．
   最後に，6ヶ月児に対する IDS と対ペット発話（PDS） [24]_ は高いピッチや大きな影響を与える部分（例えば，イントネーションやリズム）という面ではよく似ている．
   しかし，IDSのみが， ハイパーアーテキュレートな母音を含んでおり，人間の乳児において言語の出現中に実用的な機能と言語教育機能の両方を補助している可能性が最も強い．
   したがって，IDSの韻律は非言語的な方法で両親とコニュニケーションをするために影響を及ぼし，意図を使えるために重要であると思われる．

In motherese, prosodic and phonetic cues highlight syntax and lexical units, and prosody provides cues regarding grammatical units at utterance boundaries and even at utterance-internal clause boundaries [7]_ . 
Indeed, mothers reading to their children lengthen vowels for content words [25]_  and function words when they appear in a final position [26]_ .  
Mothers also position target words on exaggerated pitch peaks in the utterance-final position [27]_  but lengthen final syllables, even in utterance-internal positions [28]_ ,

.. note::

   マザリーズでは韻律的，音素的キューが構文や語彙単位を協調し，韻律は発話内部の句境界であっても発話境界の文法的な単位についてのキューを提供する．
   確かに，母親が子供に読み上げる際には内容語や [25]_ 終端にある機能語の母音を長くする [26]_ ．
   母親は発話の最終点で強調されたピッチのピークにおいてターゲットワードを配置もするが [27]_ ，発話の内部の位置においても最終シラブルを延長する [28]_ .

Although IDS analyses generally focus on supra-segmental prosodic cues, recent works aiming to computerize the recognition of motherese show that IDS’s segmental and prosodic characteristics are intertwined [29]_ [30]_ . 
The vocalic and consonantal categories are enhanced even when controlling for typical IDS prosodic characteristics [31]_ .  
Throughout the first 6 months, the vowel space is smaller and the vowel duration is longer, with some consonants also differing in duration and voice onset time. 
These characteristics may enhance both auditory and visual aspects of speech [32]_ , [33]_ [34]_ . 
Along with acoustic characteristics, visual cues seem to be a part of motherese, which suggests that hyperarticulation in natural IDS may visually and acoustically enhance speech. 
Indeed, lip movements are larger during IDS than ADS [35]_ .

.. note::

   IDS は一般的に前掲の文節あるいは韻律的キューに焦点を当てて分析をしているが，
   母親の認知のモデリングを目指す最近の研究で IDS は文節的，韻律的特性が密接に絡み合っていることを示している．
   母音と子音のカテゴリが一般的なIDSの韻律特性を制御する際に向上する [31] .
   最初の6ヶ月にわたって，母音空間は狭く，母音の持続時間は長く，いくつかの子音に関しては持続時間やボイスオンセットの時間も異なる．
   これらの特性は，音声の聴覚的，視覚的両側面を強化することができる [32]_ [33]_ [34]_ .
   音響的特性に加えて，視覚的なキューはマザリーズの一部と思われ，自然なIDSのハイパーアーテキュレートを視覚的，音響的に高めることができる．
   確かに唇の動きはADSよりIDSの方が大きくなっている．

3: Variations in motherese characteristics (マザリーズの特徴のバリエーション)
--------------------------------------------------------------------------------

3.1. According to language (言語的側面)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Specific forms of IDS are evident across various languages, including Western European languages [11]_ [36]_ [37]_ , Hebrew [38]_ , Korean [39]_ , Mandarin [40,41]_ , Japanese [42]_  and even American Sign Language (ASL) 
between deaf mothers and their deaf children [43]_ [44]_ [45]_ . 
Although general trends in the form of IDS exist, they may be mediated by linguistic and cultural factors. 
French, Italian, German, Japanese, British English and American English IDS share some general features (i.e., higher mean f0, greater f0 variability, shorter utterances, and longer pauses) 
but maintain distinct characteristics. 
For example, American IDS exhibits the most extreme prosodic modifications [11]_ , 
whereas British IDS exhibits smaller increases in vocal pitch [37]_  and has language-specific segmental distribution patterns when compared with Korean IDS [36]_ . 
Moreover, observations suggest that mothers adapt their IDS to the language-specific needs of their infants, 
for example, Japanese mothers alter phonetic cues that are more relevant in Japanese, whereas English mothers alter cues that are more relevant in English [46]_ . 
However, when a conflict arises between motherese features and language specificities (because some IDS features may disturb language salience), IDS tends to preserve the cues that are essential in ADS.  
Indeed, IDS prosody does not distort the acoustic cues essential to word meaning at the syllable level in Mandarin, which is a “tone language” [41]_ , and this is also evident for the Japanese vowel devoicing [42]_ . 
When there is a conflict between grammatical and affective facial expressions in ASL IDS, mothers shift from stressing affect to grammar around the time of their children's second birthday [45]_ .

.. note::

   IDF の特定の形状は種々の言語 (西ヨーロッパ言語 [11]_ [36]_ [37]_ , ヘブライ語 [38]_ , 韓国語 [39]_ , マンダリン [40]_ [41]_ 日本語 [42]_ , その他アメリカン手話言語（耳の聞こえない母親と同様の子供の間） [43]_ [44]_ [45]_ ) に渡り明白である。
   IDSの形の一般的傾向は存在するが、それらは言語および文化的要素によって調整されるかもしれない。
   フランス語、イタリア語、ドイツ語、日本語、英国英語、アメリカ英語の IDS は一般的な特徴量が共通している（例えば、 f0 の平均値が高い、f0 の分散が大きい、短い発話、長いポーズ）が、はっきり異なる特徴を残している。
   例えば、アメリカン IDS はもっとも極端な韻律的変化が存在するが [11]_ 、一方で英国英語では母音のピッチの上昇は少なく [37]_ , 韓国語と比較した際には言語的に特殊なセグメンタルな分布のパターンを持つ [36]_ .
   加えて、観察では母親が彼らの IDS を彼らの子供にとって言語的に必要なものに適応している。
   例えば日本の母親は音素的キューを日本語に適したなものに変化させるし、一方で英語の母親は英語に適したものに IDS を適応させる [46]_ .
   しかし、マザリーズの特徴と言語的特殊性に競合が起きた場合には（いくつかの IDS の特徴は言語の特徴点を阻害するため）、IDS は ADS に潜在的なキューを保持する。
   確かに、IDS の韻律はマンダリン（これは”トーン言語である”）のシラブルレベルでの語の意味への潜在的な音響的なキューを歪めない [41]_ し、これは日本語母音の無声化でも示されている [42]_ 。ASL-IDS において 文法的あるいは顔の表情表現の間で矛盾がある場合には、母親は子供の二回目の誕生日くらいまでは文法的に影響されたストレス型にシフトする [45]_ 。

3.2. According to infants’ age and gender (乳児の年齢、性別)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IDS quality and quantity vary as children develop. 
The mean f0 seems to increase from birth, peak at approximately 4 to 6 months, and decrease slowly until the age of two years or older [47]_ [48]_ . 
Acoustic exaggeration is also smaller in child-directed speech (CDS) than in IDS [49]_ . 
Prosodic contours vary with infants’ age [50]_ , with “comforting” prevalent between 0 and 3 months and then decreasing with age, 
“expressing affection” and “approval” peaking at 6 months and being least evident at 9 months, and “directive” utterances, which are rare at birth, peaking at 9 months of age [47]_ . 
This is consistent with a change in pragmatic function between 3 and 6 months of age, as parental speech becomes less affective and more informative [3]_ . 
Variations in the mean length of utterances (MLU) are more controversial, and Soderstrom emphasized that some properties, such as linguistic simplifications, could be beneficial at one age but problematic at another age. 
In fact, two small sample studies suggest that mothers adjust their IDS as a function of their children's language ability. 
Around the two-word utterance period, an adult-like conversational style with frequent overlaps emerges in Japanese IDS [51]_ , which has a mean f0 that reaches approximately the same value as that of ADS [52]_ . 
Mothers may continue to adjust their speech syntax to their children’s age and to the child’s feedback as children grow older [53]_ , 
but more longitudinal studies investigating the evolution of the linguistic aspects of IDS are needed.

.. note::

   IDS の質と量は子供の発達に従って変化する。
   f0 の平均値は誕生とともに増加するようで、約 4~6 ヶ月でピークになり、二歳以上になるまでにゆっくりと低下する [47]_ [48]_ .
   音響的強調も IDS よりも CDS は小さい [49]_ .
   韻律的な外郭も乳児の年齢によって異なり [50]_ , 0~3 ヶ月までは普遍的に"心地よい"感じだが、それ以降は減少していく。
   "明瞭な愛着"や"肯定感”は 6 ヶ月でピークになり、9 ヶ月までは続き、”直接的な”発話は, 誕生時には少ないが、9ヶ月にはピークに達する [47]_ .
   これは 3~6 ヶ月の実用的な機能の変化と一致しており、個人的な発話は感情的ではなくなり情報的になる [3]_ .
   発話の平均的な長さ( Mean Length of Utterance: MLU) はより話題になり、Soderstrom らはいくつかの特徴、例えば、言語的な単純化、は一歳までは有益だが、それ以上では問題になることを強調している。
   実際、二つの小さな事例研究では母親は IDS を彼らの子供の言語的な能力の機能に応じて変化させていることを示唆している
   二語発話期周辺では、しばしば重複の起きる大人のような会話様式が日本語 IDS では起きる [51]_ し、 ADS と同じような値に達する f0 平均値を持つ [52]_ .
   母親は子供が成長するに従って、子供の年齢やフィードバックに合わせた発話文法の変化をするようになる [53]_ が, IDS の言語的な側面の進化を調査するより長期的な研究が必要である。 

Infants’ gender may also modify IDS characteristics. 
When using IDS with their 0 to 12-month-old infants, Australian mothers used higher f0 and f0 ranges and more rising utterances for girls than boys, 
whereas Thai mothers used a more subdued mean f0 and more falling utterances for girls than boys [54]_ . 
Given that the gender of the infant is not neutral in interactional processes (see, for example 55), its impact on motherese should be further explored in motherese studies.

.. note::

   乳児の性別も IDS の特徴を変化させる。
   乳児が 0~12 ヶ月で IDS を使用している際、オーストラリアの母親は男子よりも女子に対して、より高い f0, f0 の範囲、上昇した発話を行い、タイの母親も男子よりも女子に対し、
   より柔らかな f0 の平均値と発話の下降を行う [54]_ 。
   乳児の性差はインタラクションのプロセスにおいて自然でないことを考慮すれば、マザリーズにおける影響はマザリーズ研究の将来の調査課題になるだろう。
 

3.3. According to infant vocalizations, ability and reactivity (乳児の発話、能力と準備に関して)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Gleason suggested that children’s feedback helps shape the language behavior of those who speak to them [56]_ . 
Indeed, Fernald, in a comparison of IDS, simulated IDS (to an absent baby) and ADS, showed that an infant’s presence facilitates IDS production. 
In simulated IDS, the mean f0 did not rise significantly compared with that of ADS, and other features, though they differed significantly from ADS, were intermediately between those of IDS and ADS [9]_ . 
In fact, IDS is dynamically affected by infants’ feedback. 
For example, IDS is reduced when the contingency of an infant’s responses is disturbed by decoupling TV sequences of the mother-infant interaction [57]_ . 
Furthermore, mothers produce higher IDS pitch when, through an experimental manipulation, 
IDS high pitch seems to elicit infants’ engagement, compared to another manipulation in which low pitch seems to strengthen infant’s engagement [58]_ . 
Mothers may also match their pitch to infants’ vocalizations. 
In the first 3 months, IDS and infants’ vocalizations are correlated in pitch, and even melody types are correlated in some mother-infant pairs [59]_  with tonal synchrony [60]_ . 
This correlation may be due to the parents, given that, in a longitudinal case study, parents consistently adjusted their vocal patterns to their 3 to 17-month-old infants’ vocal patterns, 
whereas infants did not adjust their vocal patterns to their parents’ vocal patterns [61]_ .

.. note::

   Gleason は子供のフィードバックが彼らに話かけられる言語行動の決定を手助けすることを示唆している [56]_ .
   加えて、 Fernald は、 IDS の比較において、不在の乳児に対するシミュレーションされた IDS と ADS で乳児の存在が IDS の生成に重要であることを示した。
   シミュレーションされた IDS は f0 の平均値は ADS と比較して有意に上がることはなく、他の特徴に関しては有意な差は出るものの ADS と IDS の中間的なものであった [9]_ 。
   実際、 IDS は乳児のフィードバックに動的に影響される。
   例えば、 乳児の反応が, TV によって母子インタラクションが阻害されると IDS は減少する [57]_ .
   加えて、母親は, 実験的に操作され 高い IDS ピッチを生成する際に IDS の高いピッチは乳児の関与を誘発し、その他低いピッチの操作に比べて、乳児の関与を誘発すると思われる [58]_ 。
   母親は乳児の発話能力に応じてピッチを変化させているようだ。
   最初の三ヶ月では、 IDS と乳児の発声能力はピッチにおいて相関があり、メロディータイプ [59]_ や全体の同調性 [60]_ もいくつかの母子間では相関する。
   縦断ケース研究において、両親は一貫し3-17ヶ月の乳児に合わせてボーカルパターンを調整していたのに対して、
   乳児は親のボーカルパターンに合わせることがなかったことを考慮すると、この相関は両親が原因であると思われる [61]_ .

In addition, mothers adapt their IDS to infants’ abilities and needs. 
A number of studies have shown that mothers strengthen their IDS according to the perceived lack of communicative abilities of their child. 
Although full-term infants more often followed their mothers’ utterances with a vocalization than preterm infants did, 
mothers of premature babies more often followed their infants' vocalizations with an utterance directed at the infants than did mothers of full-term babies [62]_ . 
A mother of two 3-month-old fraternal twins accommodated her IDS by using a higher mean f0 and rising intonation contours when she spoke to the infant whose vocal responses were less frequent [63]_ . 
Similarly, playing with a Jack-in-the-box, mothers exclaimed in surprise with a higher pitch when their children did not show a surprise facial expression. 
Infants’ expressions were a stronger predictor of maternal vocal pitch than their ages [64]_ . 
Mothers interacting with an unfamiliar deaf 5-year-old child used more visual communicative devices, touches, simpler speech, and frequent initiations than when communicating with an unfamiliar hearing 4.5-year-old child. 
Although each initiation toward the deaf child was less successful than the previous one, interactions occurred as frequently as with the hearing child [65]_ . 
Finally, parents of children with Down syndrome (which is a visible disability) spoke with a significantly higher f0 mean and variance than did parents of children with other types of mental retardation [66]_ .

.. note::

   加えて、母親は彼らの IDS を乳児の能力や需要に適応させている。
   いくつかの研究では母親が子供の会話能力の知覚上の欠損に応じて IDS を強調していることが示されている。
   満期出産乳児は、早産の乳児がするよりも、より頻繁に母親の発話に続いて声を出したが、早期出産乳児の母親は、満期出産乳児の母親が行うよりも、頻繁に乳児の発声に続いて乳児に対する発話を行った [62]_ 。
   二人の三ヶ月の双子を持つ母親は、あまり発話反応の多くない乳児に話しかけを行う際、f0 の平均値やイントネーションの上昇輪郭を使用して IDS を順応させた [63]_ 。
   同様に、ジャックインザボックスで一緒に遊び、 子供たちが驚きの表情を示さなかった際には、母親はより高いピッチで驚いて叫んだ。
   子供の年齢よりも母親の音声のピッチを予測しうる因子は子供の表情であった [64]_ .
   母親は不慣れな聴覚障害の５歳児とインタラクションをかわす際、より視覚的なコミュニケーションツール、例えば、接触や簡単な会話、頻繁なイニシエーションを、4.5ヶ月の聴覚障害児とコミュニケーションをとる時よりも使用した。
   なお、聴覚障害児に向けての各イニシエーションは、前者と比べ、あまり成功なかったが、相互作用は健常児の場合と同様の頻度で発生した。
   最後に、 ダウン症の子どもを持つ親（これは見える障害である） は精神遅滞、他のタイプの子供を持つ親に比べ、有意に高いF0平均と分散で話をした.

Mothers tailor their communication to their infants' levels of lexical-mapping development. 
When teaching their infants target words for distinct objects, mothers used target words more often than non-target words in synchrony with the object’s motion and touch. 
This mothers’ use of synchrony decreased with infants' decreasing reliance on synchrony as they aged [67]_ . 
Similarly, IDS’s semantic content shows strong relationships with changes in children's language development from zero to one-word utterances [68]_ , 
and a clear signal of non-comprehension from children results in shorter utterances [69]_ . 
In the IDS directed toward their profoundly deaf infants with cochlear implants, mothers tailored pre-boundary vowel lengthening to their infants' hearing experience 
(i.e., linguistic needs) rather than to their chronological age, yet they all exaggerated the prosodic characteristics of IDS (i.e., affective needs) regardless of their infants' hearing status [70]_ [71]_ . 
Thus, we conclude that IDS largely depends on the child given that it increases with infants’ presence and engagement, 
is influenced by infants’ actual preferences and vocalizations and depends on mothers’ perceptions of their infants’ overall abilities and needs.

.. note::

   母親は乳児の語彙マッピングの発達レベルに応じてコミュニケーションを適応させる。
   乳児に異なるオブジェクトの目的語を教える際に、母親はオブジェクトの動きと接触の同機を非目的語よりも目的語に対してより多く使用した。
   母親の同機に対する依存は、乳児が年齢とともに同機依存を減少させると減少していった。
   同様に、 IDS の意味内容は乳児の0語から1語発話への言語的な発達と子供からの非理解的で明確
   人工内耳をもつ重度聴覚障害乳幼児向けのIDSでは、 母親は彼らの暦年齢よりも、 むしろ乳児の聴覚経験の前境界母音延長（すなわち、言語的ニーズ）に応じた。
   一方で IDS の韻律的特徴は（すなわち感情のニーズ）は乳児の聴覚の状況に関係なく、すべて強調した。
   そのため、我々は、乳幼児のプレゼンスと関与とともに増加することを考えると 
   IDS は乳幼児の影響を受けて「実際の好みや発声や母親に依存し彼らの乳児の全体的な能力やニーズの認識されている」と結論づけた。

3.4. Do parental individual differences modify motherese quality? (両親の個人差はマザリーズの質に影響するのか)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Whether a mother had siblings could explain some individual variability in IDS, 
given that women who grew up with siblings were more likely to show prosodic modifications when reading picture books to a young child than those who did not have siblings [72]_ . 
Social class and socio-economic status (as measured by income and education) impact mothers' CDS [73]_ [74]_ [75]_ , and this impact is mediated by parental knowledge of child development [75]_ . 
However, main effects of communicative setting (e.g., mealtime, dressing, book reading, or toy play) and the amount of time that mothers spend interacting with their children may be important influences [74]_ .

.. note::

   母親が兄弟を持っているか否かは IDS の個人差の多様性の一部を説明するかもしれない。
   兄弟と一緒に育った女性は兄弟のいない女性に比べ、乳児に絵本を読み聞かせする際に韻律的変化を示す可能性が高い [72]_ 。
   社会階級と経済的状態 (収入と教育によってはかられた)は母親の CDS に影響し [73]_ , [74]_ , [75]_ , この影響は両親の子どもの発達に関する知識に仲介される [75]_ 。
   しかし、コミュニケーションの準備（例えば、食事の時間や着替え、本の読み聞かせ、もしくはおもちゃで遊ぶ等）の主効果や
   母親や子どもとインタラクションをかわす時間は重要な影響力を持っている [74]_ 。

Neural and physiological factors may be relevant to the parenting of young children and to IDS production. 
When listening to IDS, mothers of preverbal infants (unlike mothers of older children) showed enhanced activation in the auditory dorsal pathway of the language areas in functional MRIs.  
Higher cortical activation was also found in speech-related motor areas among extroverted mothers [76]_ . 
Additionally, in the first 6 months, the maternal oxytocin level is related to the amount of affectionate parenting behavior shown, 
including "motherese" vocalizations, the expression of positive affect, and affectionate touch [77]_ .
Finally, maternal pathology may affect IDS. 
The influence of maternal depression on IDS has been the main focus of previous studies. 
Results show that depressed mothers fail to modify their behavior according to the behavior of their 3 to 4month-old infants, are slower to respond to their infants’ vocalizations, and are less likely to produce motherese [78]_ .
Depressed mothers also speak less frequently with fewer affective and informative features with their 6 and 10-monthold infants, and the affective salience of their IDS fails to decrease over time [79]_ . 
Moreover, depressed mothers show smaller IDS f0 variance except when taking antidepressant medication and being in partial remission [80]_ . 
Mothers with schizophrenia also show less frequent use of IDS compared to other mothers with postnatal hospitalizations [81]_ .

.. note::

   脳科学的あるいは心理学的因子は乳児のしつけやIDSの発声と関係がある。
   IDS を聞く際には、まだ言語を話す前の乳児の母親は（それより年上の乳児の母親とは異なり）, ファンクショナル MRIs における言語野の音響的背面経路の活性度の強調が見られる。
   高次皮質の活性度も外交的な母親の間で発話に関係するモーター野において発見されている [76]_ .
   加えて最初の六ヶ月に関して母方のオキシトシンレベルは愛情のこもったしつけ行動の量、例えばマザリーズのボーカリゼーションや肯定的に影響する表現、
   親密な摂食行動と関係している [77]_ 。
   最後に、母親の病気もIDSに影響する [78]_ .
   IDS における母親の鬱病の影響は先行研究における主題の一つであった。
   鬱病の母親は彼女らの行動を３−４ヶ月児に合わせて変化させることが少なく、IDS の目立った特徴を長い間変化させないという結果を示している [79]_ 。
   さらに、鬱病の母親は IDS の f0 の変化が、抗うつ剤を接種している場合や一時的に鎮静している場合をのぞいて、少ないことを示している [80]_ 。
   統合失調症をもつ母親もまた、IDS の使用が他の産後入院をしている母親と比較して、少ないことが示されている [81]_ 。

Thus, in addition to factors associated with the infants, various maternal factors (i.e., familial, socio-economic, physiological, and pathologic) can modulate IDS production.  

.. note::

   従って、乳児と結びついた要素に加えて、 母親側の種々の要素 (例えば、経済状態や、心理学的、病理学的な状態) も IDS 発話を変調させうる。

4: Motherese effects on the infant (乳児に対するマザリーズの影響)
------------------------------------------------------------------------

As hypothesized, IDS may function developmentally to communicate affect, regulate infants’ arousal and attention, and facilitate speech perception and language comprehension [16]_ [82]_ .

.. note::

   仮説として、IDSはコミュニケーション効果のある発達的な機能を持っており、通常の乳児を覚醒させ、注意を引き、発話の知覚や言語の理解を手助けする [16]_ [82]_ .

4.1. Communication of affect and physiological effects (コミュニケーションの影響と整理学的影響)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Though communication of affect is crucial with regard to communicating with very young infants without linguistic knowledge, few studies have addressed it. 
Despite the lack of available studies, IDS may convey mothers’ affect and influence infants’ emotions. 
As reported previously, prosodic patterns are more informative in IDS, and the variations in prosodic contours provide infants with reliable cues for determining their mothers’ affect and intentions. 
Indeed, when hearing an unfamiliar language in IDS, 5-month-olds smile more often to approvals and display negative affect in response to prohibitions, and these responses were not evident in ADS [83]_ . 
Similarly, IDS approval contours elevate infants’ looking, whereas disapproval contours inhibit infants’ looking [84]_ . 
Also 14-18 months old infants use prosody to understand intentions [85]_ . 
At a psycho-physiological level, a deceleration in heart rate was observed in 9-month-old infants listening to IDS, and EEG power, specifically in the frontal region, was linearly related to the affective intensity of IDS [86]_ . 
Finally, one study [87]_  reported an astonishing physiological correlation: 3 to 4month-old infants (N=52) grew more rapidly when their primary caregivers spoke high quality/quantity IDS. 
This could be influenced by other intermediate physiological factors, but this work needs to be replicated.

.. note::

   コミュニケーションの影響は言語の知識のない、幼い乳児とのコミュニケーションに関して致命的であり、いくつかの研究ではこれを特定した。
   利用可能な研究が欠如しているが、 IDS は母親の感情を伝え、乳児の情動に対する影響するだろう。
   今までの報告として、韻律パターンは IDS のほうがより豊富な情報をもち、韻律変化における多様性は、乳児に母親の感情や意図を推定するために利用可能な手がかりを提供する。
   加えて、IDS でなじみのない言語を聞いた際には、５ヶ月児は承認に対してより微笑みを返し、否定的な感情を提示すると拒絶的な反応を返した。
   また、これらの反応は ADS では起きなかった [83]_ 。
   同様に、IDS の肯定的変化は乳児の視線をより向けさせるし、否定的な変化は乳児の視線をそらさせる [84]_ .
   14-18 ヶ月の乳児も意図の理解に韻律を使用する [85]_ 。
   心と体の相互作用レベルでは、9 ヶ月児が IDS を聞く際には心拍数の低下が観察された。
   また、 EEG power 、特に前頭葉、は直線的に IDS の感情の強さと関連している [86]_ .
   最後に、ある研究 [87]_ では思いがけない心理的な相互関係が報告されている。
   3-4 ヶ月児（N=52）は彼らの主要な養育者が高い質と量の IDS を発話する際により急速に成長した。
   これは他の中間的な心理学的因子の影響を受けている可能性もあり、この研究には追試が必要である。

4.2. Facilitation of social interactions through infants’ preference for IDS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Infants prefer to listen to IDS when compared to ADS [88]_ , and they show greater affective responsiveness to IDS than ADS [89]_ . 
This finding is also evident for deaf infants seeing infant-directed signing [44]_  and even for severely handicapped older hearing children [89]_ . 
Moreover, infants remember and look longer at individuals who have addressed them with IDS [90]_ . 
Finally, this greater responsiveness makes them more attractive to naïve adults, which helps maintain positive adultinfant interactions [91]_ .

Infants’ preferences follow a developmental course, on that they are present from birth and may not depend on any specific postnatal experience (though prenatal auditory experience with speech may play a role). 
One-month-old and even newborn infants prefer the IDS from an unfamiliar woman to the ADS from the same person [92]_ [93]_ [94]_ . 
While neonates sleep, the frontal cerebral blood flow increases more with IDS than with ADS, which suggests that IDS alerts neonates’ brains to attend to utterances even during sleep [95]_ . 
IDS preferences change with development, in that the preference for IDS decreases by the end of the first year [96]_ [97]_ . 
Thereafter, infants may be more inconsistent, in that one study found a preference for IDS [96]_  but another did not [97]_ . 
Thus, more studies are needed to understand the precise course of infants’ preferences for IDS after 9 months of age. 
With regard to the speech of their own mothers, only 4-month-old infants (and not 1-month-olds) prefer IDS to ADS [98]_ , and newborns prefer their mothers' normal speech to IDS [99]_ . 
With regard to the quality of IDS, infants’ preferences also follow a developmental course. 
Fourmonth-olds prefer slow IDS with high affect, whereas 7-montholds prefer normal to slow IDS regardless of its affective level [100]_ . 
The developmental course of infants' preferences is consistent with the type of affective intent used by mothers at each age [47]_ . 
The terminal falling contour of IDS (e.g., a comforting utterance) may serve to elicit a higher rate of vocal responses in 3-month-old infants [101]_ . 
Infants’ preferences shift between 3 and 6 months from comforting to approving, and between 6 and 9 months from approving to directing [102]_ .  
Rising, falling, and bell-shaped IDS contours arouse 4 to 8month-olds’ attention [103]_ . 
However, 6-month-olds, but not 4month-olds, are able to categorize IDS utterances into approving or comforting [104]_ . 

Finally, adults prefer ADS (i.e., in content and prosody) to IDS [105]_ .
What are the acoustic determinants of infants’ preference for IDS? 
When lexical content is eliminated, young infants show an auditory preference for the f0 patterns of IDS, but not for the amplitude (correlated to loudness) or duration patterns (rhythm) of IDS [88]_ [106]_ [107]_ . 
This pattern is consistent with the finding that infants prefer higher pitched singing [108]_ . 
However, deaf infants also show greater attention and affective responsiveness to infant-directed signing than to adult-directed signing [44]_ . 
Although an auditory stimulus with IDS characteristics was more easily detected in noise than one that resembled ADS characteristics [109]_  and mothers accentuate some IDS characteristics in a noisy context [97]_ , 
infants’ preference is independent of background noise [97]_ . 
Actually, IDS preference relies on a more general preference for positive affect in speech. 
When affect is held constant, 6-month-olds do not prefer IDS. 
They even prefer ADS if it contains more positive affect than IDS. 
Having a higher and more variable pitch is neither necessary nor sufficient for determining infants' preferences, although f0 characteristics may modulate affectbased preferences [110]_ . 
This result may be linked with the finding that IDS’s prosody is driven by the widespread expression of emotion toward infants compared with the more inhibited manner of adult interactions [22]_ . 
However, though this issue may be very fruitful for future study, as was evident in the previous section, 
there is currently a lack of studies addressing the affective and emotional effects of motherese 
(for example, the immediate effects on infants’ expressions, variations according to infants’ age, later effects on infants’ attachment, and so on, as for mother-infant synchrony, 
the immediate and later effects of which are now well documented). 
In contrast, many studies simply address the more behavioral concept of “infants’ preference” for motherese.  Finally, preferences depend on linguistic needs. 
Six-montholds, for example, prefer IDS that is directed at older infants when the frequency of repeated utterances is greater, thus matching the IDS directed at younger infants [111]_ . 
This preference for repetitiveness may explain why 6-month-olds prefer audiovisual episodes of their mothers singing rather than speaking IDS [112]_ [113]_ .

In summary, the preference for IDS, which is characterized by better attention, gaze and responsiveness from infants, is less prevalent for the infant’s own mother, and is generally related to the affective intensity of the voices. 
Moreover, this preference is modulated by the age of the infant, which is most likely due to infants’ affective and cognitive abilities and needs.

4.3. Arousing infants’ attention and learning
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IDS has arousing properties and facilitates associative learning. 
In contrast to ADS, IDS elicits an increase in infants’ looking time between the first and second presentations.  
Similarly, when alternating ADS and IDS, infants’ responses to ADS are stronger if preceded by IDS, whereas their responses to IDS are weaker if preceded by ADS [114]_ . 
In a conditionedattention paradigm with IDS or ADS as the signal for a face, only IDS elicited a significant positive summation, and only when presented with a smiling or a sad face (not a fearful or an angry one) [115]_ . 
IDS may in fact serve as an ostensive cue, alerting a child to the referential communication directed at him or her. 
Eye-tracking techniques revealed that 6-month-olds followed an adult's gaze (which is a potential communicativereferential signal) toward an object (i.e., joint attention) 
only when it was preceded by ostensive cues, such as IDS or a direct gaze [116]_ . 
Likewise, the prosodic pattern of motherese (which is similar to other cues such as eye contact, saying the infant’s name and contingent reactivity) 
triggered 14-montholds to attend to others' emotional expressions that were directed toward objects [117]_ . 
Thus, IDS may help infants learn about objects from others and, more specifically, about others’ feelings toward these objects, which may pave the way for developing a theory of mind and intersubjectivity.

Yet, experience-dependent processes also influence the effects of IDS. 
Kaplan conducted several studies using the same conditioned-attention paradigm with face reinforcers to assess how parental depression affected infants’ learning. 
We know that depression reduces IDS quantity [78]_  and quality [80]_ , 
which may explain why infants of depressed mothers do not learn from their mother’s IDS yet still show strong associative learning in response to IDS produced by an unfamiliar, non-depressed mother [118]_ [119]_ . 
However, this learning was poorer when maternal depression lasted longer (e.g., with 1-year-old children of mothers with perinatal onset) [120]_ . 
Nevertheless, infants of chronically depressed mothers acquired associations from the IDS of non-depressed fathers [121]_ . 
Paternal involvement may also affect infants’ responsiveness to male IDS. 
In contrast with infants of unmarried mothers, infants of married mothers learned in response to male IDS, especially if their mothers were depressed [122]_ . 
However, as expected, infants of depressed fathers showed poorer learning from their fathers' IDS [123]_ .  Finally, current mother-infant interactions influence infants’ learning from their mothers’ IDS. 
In fact, f0 modulations, though smaller in depressed mothers’ IDS, did not predict infants’ learning, whereas maternal sensitivity did, even when accounting for maternal depression [124]_ . 
In summary, IDS learning facilitation is affected by past and current experiences (such as long durations of time with a depressed mother, having an involved father, and having a sensitive mother).

4.4. Facilitation of language acquisition
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

4.4.1. Does IDS’s prosody aid in language acquisition, and, if so, how?
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

The supra-segmental characteristics of IDS (i.e., f0 amplitude and duration) can facilitate syllable discrimination [125]_ [126]_ . 
When given vowel tokens that were drawn from either English or Japanese IDS, an algorithm successfully discovered the language-specific vowel categories, 
thereby reinforcing the theory that native language speech categories are acquired through distributional learning [127]_ . 
Trainor observed that, although the exaggerated pitch contours of IDS aid in the acquisition of vowel categories, 
the high pitch of IDS might impair infants' ability to discriminate vowels (thereby serving a different function, such as attracting infants' attention or aiding in their emotional communication) [128]_ .  
Nevertheless, IDS’s prosody facilitates syllabic discrimination and vowel categorization in the first 3 months.

IDS’s prosody may also help pre-linguistic infants segment speech into clausal units that have grammatical rules, 
and the pitch peaks of IDS, especially at the ends of utterances, may assist in word segmentation and recognition, which facilitates speech processing. 
Indeed, 7 to 10-month-olds prefer to listen to speech samples that are segmented at clause boundaries than to samples with pauses inserted at within-clause locations [129]_ , 
but this was only for IDS samples, not for ADS samples [130]_ . 
Infants can distinguish words from syllable sequences that span word boundaries after exposure to nonsense sentences spoken with IDS’s prosody, but not with ADS’s prosody [131]_ . 
Moreover, mothers of 20-month-old late-talkers marked fewer nouns with a pitch peak and used more flat pitch contours than mothers of typical children [132]_ . 
In a review of previous research, Morgan suggested that prosody is an important contributor to early language understanding and assists infants in developing the root processes of parsing [133]_ .

Stress information shapes how statistics are calculated from the speech input and is encoded in the representations of the parsed speech sequences. 
For example, to parse sequences from an artificial language, 7 and 9-month-olds adopted a stress-initial syllable strategy and appeared to encode the stress information as part of their proto-lexical representations [134]_ . 
In fluent speech, 7.5-month-olds prefer to listen to words produced with emphatic stress, although recognition was most enhanced when the degree of emphatic stress was identical during familiarization and recognition tasks [135]_ . 
Does word learning with IDS’s prosody impair word recognition in ADS?  
The high affective variation in IDS appears to help preverbal infants recognize repeated encounters with words, which creates both generalizable representations and phonologically precise memories for the words. 
Conversely, low affective variability appears to degrade word recognition in both aspects, thereby compromising infants' ability to generalize across different affective forms of a word and detect similar sounding items [136]_ . 
Automatic isolated-word speech recognizers trained on IDS did not always generate better recognition performances, 
but, for mismatched data, their relative loss in performance was less severe than that of recognizers trained on ADS, which may be due to the larger class overlaps in IDS [137]_ . 
Additionally, 7 to 8-month-old infants were successful on word recognition tasks when words were introduced in IDS and not successful for those introduced in ADS, regardless of the register of recognition stimuli [138]_ . 
Furthermore, IDS may be more easily detected than ADS in noisy environments [109]_ .  Finally, clarity may vary with the age of the listener. 
Having a slow speaking rate and vowel hyper-articulation improved 19month-olds’ ability to recognize words, but having a wide pitch range did not [139]_ . 
For adult listeners, words that were isolated from parents’ speech to their 2 to 3-year-olds were less intelligible than words produced in ADS [140]_ .

Thus, IDS’s prosody facilitates vowel categorization, syllabic discrimination, speech segmentation in words and grammatical units, and word recognition. 
Moreover, IDS’s prosody may serve as an attentional spotlight that increases brain activity to potentially meaningful words [141]_ . 
Indeed, event-related potentials increased more for IDS than ADS (only in response to familiar words for 6-month-olds and to unfamiliar words for 13-month-olds).

4.4.2. Do the linguistic properties of IDS aid in language acquisition, and, if so, how?
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

In response to both Chomsky’s view that motherese is a form of degenerate speech and the resulting theoretical impetus toward nativist explanations of language acquisition, 
several researchers have sought for evidence that language input to children is highly structured and possibly highly informative for the learner. 
There has been a lively debate between the proponents of motherese as a useful tool for language acquisition and those who contend that it does not aid language acquisition. 
First, Newport [142]_  claimed that motherese is not a syntax-teaching language, given that it may be an effect rather than a cause of learning language. 
Newport and colleagues found few correlations between the syntax evident in caregivers’ speech and language development.  
Responding to Furrow [143]_ , one study with two groups of agematched children (18 to 21-month-olds and 24 to 27-montholds) also found few effects of the syntax of mothers’ IDS on children's language growth, 
with most effects restricted to a very young age group, which suggested that the complexity of maternal speech is positively correlated with child language growth in this age range [144]_ . 
Scarborough [145]_  also found that maternal speech type did not influence language development.

However, other studies that considered children's level of language at the time of maternal speech assessment found a relationship between maternal IDS’s semantic and syntactic categories and children’s language development. 
Several characteristics (e.g., MLU and pronoun use) of mothers’ IDS with their 18-month-olds predicted the children’s subsequent (27-month-old) speech, 
specifically, the mothers' choice of simple constructions facilitated language growth [143]_ [146]_ .  
Rowe, in a study controlling for toddlers’ previous vocabulary abilities, found that CDS at 30 months of age predicted children’s vocabulary ability one year later [75]_ . 
As early as 13 months of age, pre-existing differences were found between mothers of earlier and later talkers. 
When individual differences in style of language acquisition (i.e., expressive versus nonexpressive styles) were examined, 
several associations emerged for the “non-expressive” group between the IDS type at 13 months of age and the mean length of utterance at 20 months of age [147]_ .

Which linguistic characteristics of motherese may aid in language acquisition? First, the statistically prominent structural properties of CDS that may facilitate language acquisition are present in realistic CDS corpora [148]_ . 
In particular, the partial overlap of successive utterances, which is well known in CDS, enhances adults’ acquisition of syntax in an artificial language [149]_ . 
CDS contains isolated words and short, frequently used sentence frames. 
A familiar sentence context may aid in word acquisition given that 18-month-olds are slower to interpret target words (i.e., familiar object names) in isolation than when these words are preceded by a familiar carrier phrase [150]_ .  
The tendency in IDS to put target words in sentence-final positions may help infants segment the linguistic stream. 
When hearing IDS in Chinese, English-speaking adults learned the target words only when the words were placed in the final position, and this was not when they were placed in a medial position [151]_ . 
Finally, the use of diminutives (a pervasive feature of CDS that is evident in many languages) facilitates word segmentation in adults hearing an unfamiliar language [152]_ [153]_ , 
and enhances gender categorization [154]_  and gender agreement even in languages that uses few diminutives [155]_ .

In summary, results support the idea that prosodic and linguistic aspects of IDS play an important role in language acquisition. 
One possibility is that prosodic components play a major part in the very early stages of language acquisition and linguistic aspects play an increasingly important part later in development when children gain some verbal abilities.  

Discussion (ディスカッション)
================================

1: Summary
----------

Our review has some limitations. 
Some studies may have not been identified because not recorded in our 2 databases.  Some studies without significant results may have not been reported (risk of a publication bias). 
And some results of included studies may be considered with caution because they don’t have been replicated or they sometimes derive from a little sample of participants. 
Some highlights emerge from this review, however. 
IDS transcends specific languages. 
Mothers, fathers, grandmothers and other caregivers all modify their speech when addressing infants, and infants demonstrate a preference for IDS. 
Nonetheless, various factors related either to the caregiver or to the infant influence the quality of the IDS.  
If present from birth, IDS, like an infant’s preference for IDS, follows a developmental course that can be influenced by the infant’s experience (see Kaplan’s work). 
IDS consists of linguistic and supra-linguistic modifications. 
The linguistic modifications include shorter utterances, vocabulary and syntactic simplifications, and the use of diminutives and repetition, all of which are designed to facilitate comprehension and aid in language acquisition. 
Prosodic modifications may serve more ubiquitous functions. 
Using a higher pitch matches infants’ preferences, and, using a wider f0 range may facilitate infants’ arousal and learning. 
Prosodic contours convey caregivers’ affect and intentions, and some of these contours stimulate infants’ responsiveness. 
Finally, exaggerated pitch contours and phonetic modifications facilitate vowel discrimination, phonetic categorization, speech segmentation, word recognition and syntax acquisition.

2: Positioning IDS within a More Global Communication Phenomenon
----------------------------------------------------------------

We observed that mothers adjust their IDS to their infants’ abilities. 
From a broader communications perspective, IDS may be part of a more general phenomenon of adaptation to a partner during communication. 
First, other cases of speech adjustment to the listener exist. 
Adults simplify their vocabulary choices when speaking with children who are up to 12 years of age [156]_ . 
In speech directed at elderly adults, CDS (which clarifies instructions by giving them in an attention-getting manner) is often used and may improve elderly adults’ performance and arousal in difficult tasks [157]_ . 
Even in normal ADS, new words are highlighted with prosodic cues. 
In both IDS and ADS, repeated words are shorter, quieter, lower pitched, and less variable in pitch than words the first time they are mentioned, 
and they are placed in less prominent positions relative to new words in the same utterance [5]_ . 
Even in master–dog dyads, the structural properties of “doggerel” (PDS) are strikingly similar to the structural properties of motherese except in functional and social areas [158]_ . 
Second, speakers other than human mothers and caregivers adjust their speech to infants. 
Four-year-old children modify some of their prosodic characteristics when speaking to infants, in that they speak more slowly, tend to lower their f0, and change their amplitude variability [159]_ . 
The linguistic content of educational children's programs also generally follows the linguistic
constraints and adjustments that are evident in adults' CDS [160]_ . 
The use of IDS by humans has been compared with the “caregiver call” 
(which is almost exclusively infant-directed) in squirrel monkeys, 
of which the variability of several acoustic features, most notably pitch range and contour, is associated with particular contexts of infant care, such as nursing or retrieval [161]_ . 
Similarly, tamarins are calmed by music with the “acoustical characteristics of tamarin affiliation vocalizations” [162]_ . 
In a comparison of the mother-infant gestural and vocal interactions of chimpanzees and humans, Falk [163]_ , 
suggested that pre-linguistic vocal substrates for motherese evolved as females gave birth to relatively undeveloped neonates and adopted new strategies that entailed maternal quieting, 
reassuring, and controlling of the behaviors of physically removed infants (who were unable to cling to their mothers' bodies). 
The characteristic vocal melodies of human mothers' speech to infants might be biologically relevant signals that have been shaped by natural selection [164]_ , 
a finding that is integrated in a more general human and nonhuman communication field.

3: Integrating IDS into the Nature of Mother-Infant Interactions
----------------------------------------------------------------

IDS implies emotion sharing, mother-infant adjustment, synchrony and multimodal communication. 
Indeed, IDS is part of a multimodal, synchronous communication style used with infants to sustain interactions and highlight messages. 
Mothers support their vocal communication with other modalities (e.g., gestural, tactile, and visual). 
At a gestural level (“gesturese”), mothers of 16 and 20-month-old infants employ mainly concrete deictic gestures (e.g., pointing) 
that are redundant with the message being conveyed in speech to disambiguate and emphasize the verbal utterance. 
Moreover, children's verbal and gestural productions and vocabulary size may be correlated with maternal gesture production [165]_ [166]_ . 
Mothers’ demonstrations of the properties of novel objects to infants are higher in interactiveness, enthusiasm, proximity to the partner, 
range of motion, repetitiveness and simplicity, thereby indicating that mothers modify their infant-directed actions in ways that likely maintain infants' attention and highlight the structure and meaning of an action [167]_ . 
Moreover, mothers’ singing and synchronous behaviors with the beat (“songese”) segment the temporal structure of the interaction, 
such that 3to 8-month-old infants are sensitive to their mothers’ emphasis by producing more synchronous behaviors on some beats than on others. 
The multimodal sensory information provided by mothers shares the characteristics of “motherese” and may ensure effective learning in infants [168]_ . 
Mothers also use contingency and synchrony (both intrapersonal and interpersonal) to reinforce dialogues and exchanges. 
By highlighting focal words using the nonlinguistic contextual information that is available to the listener and by producing frequent repetitions and formulaic utterances, 
IDS may be a form of “hyper-speech” that facilities comprehension by modifying the phonetic properties of the individual words and providing contextual support on perceptual levels 
that are accessible to infants even in the earliest stages of language learning [169]_ . 
Pragmatic dimensions of IDS may provide contingent support that assists in language comprehension and acquisition. 
In a case study, both parents used approximately equal amounts of language with their infants, but the functions of the mother’s speech differed importantly from those of the father’s speech 
with regard to providing more interactive negotiations, which could be crucial to language development [170]_ . 
Thus, IDS appears to be a part of a maternal interactive style that supports the affective and verbal communication systems of the developing infant.

IDS should be regarded as an emotional form of speech.  
Several studies highlight the impact of emotion on both motherese production and its effects, particularly with regard to prosodic characteristics that are conditioned by vocal emotions [22]_ . 
In general, acoustic analyses of f0 are positively associated with subjective judgments of emotion [171]_ . 
Thus, prosody (which is linked with f0 values and contours) reveals affective quantity and quality. 
The literature on infants' perception of facial and vocal expressions indicates that infants’ recognition of affective expressions relies first on multimodally presented information, then on recognition of vocal expressions and finally on facial expressions [172]_ .  Moreover, IDS’s affective value determines infants’ preferences [110]_ . 
Therefore, mothers’ affective pathologies, which include maternal depression, alter motherese and impair infants’ conditioned learning with IDS. 
Could IDS, music and emotion be linked before birth through prenatal associations between a mother's changing emotional state, concomitant changes in hormone levels 
in the placental blood and prenatally audible sounds? These links may be responsible for infants’ sensitivity to motherese and music [173]_ .

Finally, IDS highlights mother-infant adjustments during interactions. 
Mothers adjust their IDS to infants’ age, cognitive abilities and linguistic level. 
Therefore, IDS may arouse infants’ attention by signaling speech that is specifically addressed to them, with content and form that are adapted for them. 
Mothers also adapt their IDS to infants’ reactivity and preferences.  
Mothers’ continuous adjustments to their infants result in the facilitation of exchanges and interactions, with positive consequences for sharing emotions and for learning and language acquisition. 
Thus, maternal sensitivity predicts infants’ learning better than f0 ranges do [124]_ . 
Infants’ reactivity is also important given that their presence increases motherese [9]_ , and infants’ positive, contingent feedback makes them more attractive [91]_ , 
which in turn increases the quality of the motherese [57]_ [58]_ . 
Mother-infant contingency and synchrony are crucial for IDS production and prolongation.

In :num:`Figure #summary-of-the-motherese-interactive-loop` , we summarize the main points that were previously discussed. 
We suggest that motherese mediates and reflects an interactive loop between the infant and the caregiver, such that each person’s response may increase the initial stimulation of the other partner. 
At the behavioral level, this interactive loop is underpinned by the emotional charge of the affective level and affects, at the cognitive level, 
attention, learning and the construction of intersubjective tools, such as joint attention and communicative skills. 
Direct evidence of this intertwinement of cognitive and interactive levels is offered by Kuhl’s finding that infants’ learning of the phonetic properties of a language requires interactions with a live linguistic partner [174]_ , 
as audiovisual input is insufficient for this. 
Regarding this impact of social interaction on natural speech and language learning, Kuhl wondered whether the underlying mechanism could be the increased motivation, 
the enriched information that social settings provide, or a combination of both factors [175]_ . 
Given that autistic children and children raised in social deprivation do not develop a normal language, Kuhl suggested that the social brain “gates” language acquisition. 
As an outcome of our review, we suggest that the co-construction that emerges from the reciprocal infant-maternal adaptation and reinforcement via the interactive loop could be crucial 
to the development of infants’ cognitive and verbal abilities, which would be consistent with humans’ fundamental social nature.

Conclusion (結論)
=======================

Some authors held the perspective that, beyond language acquisition, IDS significantly influences infants’ cognitive and emotional development (e.g., [4]_ [176]_ ). 
Our systematic review supports this view. 
More studies are needed to understand how IDS impacts affective factors in infants and how this is linked with infants’ cognitive development, however. 
An interesting approach may be to investigate how this process is altered by infants’ communicative difficulties, such as early signs of autism spectrum disorder, and how these alterations may affect infants’ development [177]_ .

.. _summary-of-the-motherese-interactive-loop:

.. figure:: fig/fig2.png
   
   Summary of the motherese interactive loop (a) and its socio-cognitive implications (2B). 

   - 1A: The motherese interactive loop implies that motherese is both a vector and a reflection of mother-infant interaction.
   - 2B: Motherese affects intersubjective construction and learning. Its implications for infants’ early socio-cognitive development are evident in affect transmission and sharing, and in infants’ preferences, engagement, attention, learning and language acquisition.  

   doi: 10.1371/journal.pone.0078103.g002

.. note:: Supporting Information (DOCX)

   Annex S1.
   Rejected papers and reasons for their exclusion.  (DOCX)
   Checklist S1. PRISMA Checklist.

.. note:: Author Contributions

   Conceived and designed the experiments: DC MC CSG MCL FM. 
   Performed the experiments: CSG RC AM FA. 
   Analyzed the data: CSG MC MCL DC. Contributed reagents/materials/ 
   analysis tools: AM MC. Wrote the manuscript: CSG DC MC AM
   FA MCL RC FM.

References (参照)
====================

.. [1] Saxton M (2008) What's in a name? Coming to terms with the child's linguistic environment. J Child Lang 35: 677-686. PubMed: 18588720.
.. [2] Ferguson CA (1964) Baby Talk in Six Languages. Am Anthropol 66: 103-114. doi:10.1525/aa.1964.66.suppl_3.02a00060.
.. [3] Soderstrom M, Morgan JL (2007) Twenty-two-month-olds discriminate fluent from disfluent adult-directed speech. Dev Sci 10: 641-653. doi: 10.1111/j.1467-7687.2006.00605.x. PubMed: 17683348.
.. [4] Snow CE, Ferguson CA (1977) Talking to children. Cambridge, UK: Cambridge University Press.
.. [5] Fisher C, Tokura H (1995) The given-new contract in speech to infants.  J Mem Lang 34: 287-310. doi:10.1006/jmla.1995.1013.
.. [6] Grieser DL, Kuhl PK (1988) Maternal speech to infants in a tonal language: Support for universal prosodic features in motherese. Dev Psychol 24: 14-20. doi:10.1037/0012-1649.24.1.14.
.. [7] Soderstrom M, Blossom M, Foygel R, Morgan JL (2008) Acoustical cues and grammatical units in speech to two preverbal infants. J Child Lang 35: 869-902. doi:10.1017/S0305000908008763. PubMed: 18838016.
.. [8] Durkin K, Rutter DR, Tucker H (1982) Social interaction and language acquisition: Motherese help you. First Lang 3: 107-120. doi: 10.1177/014272378200300803.
.. [9] Fernald A, Simon T (1984) Expanded intonation contours in mothers' speech to newborns.  Dev Psychol 20: 104-113.  doi: 10.1037/0012-1649.20.1.104.
.. [10] Ogle SA, Maidment JA (1993) Laryngographic analysis of child-directed speech. Eur J Disord Commun 28: 289-297. doi:10.1111/j.  1460-6984.1993.tb01570.x. PubMed: 8241583.
.. [11] Fernald A, Taeschner T, Dunn J, Papousek M, de Boysson-Bardies B et al. (1989) A cross-language study of prosodic modifications in mothers' and fathers' speech to preverbal infants. J Child Lang 16: 477-501. doi:10.1017/S0305000900010679. PubMed: 2808569.
.. [12] Niwano K, Sugai K (2003) Pitch Characteristics of Speech During Mother-Infant and Father-Infant Vocal Interactions. Jpn J Spec Educ 40: 663-674.
.. [13] Shute B, Wheldall K (1999) Fundamental frequency and temporal modifications in the speech of British fathers to their children. Educ Psychol 19: 221-233. doi:10.1080/0144341990190208.
.. [14] Shute B, Wheldall K (2001) How do grandmothers speak to their grandchildren? Fundamental frequency and temporal modifications in the speech of British grandmothers to their grandchildren. Educ Psychol 21: 493-503. doi:10.1080/01443410120090858.
.. [15] Nwokah EE (1987) Maidese versus motherese--is the language input of child and adult caregivers similar? Lang Speech 30 ( 3): 213-237.  PubMed: 3503947.
.. [16] Fernald A (1989) Intonation and communicative intent in mothers' speech to infants: is the melody the message? Child Dev 60: 1497-1510. doi:10.2307/1130938. PubMed: 2612255.
.. [17] Bryant GA, Barrett HC (2007) Recognizing intentions in infant-directed speech: evidence for universals. Psychol Sci 18: 746-751. doi: 10.1111/j.1467-9280.2007.01970.x. PubMed: 17680948.
.. [18] Katz GS, Cohn JF, Moore CA (1996) A combination of vocal fo dynamic and summary features discriminates between three pragmatic categories of infant-directed speech. Child Dev 67: 205-217. doi: 10.1111/j.1467-8624.1996.tb01729.x. PubMed: 8605829.
.. [19] Stern DN, Spieker S, MacKain K (1982) Intonation contours as signals in maternal speech to prelinguistic infants. Dev Psychol 18: 727-735.  doi:10.1037/0012-1649.18.5.727.
.. [20] Papoušek M, Papoušek H, Symmes D (1991) The meanings of melodies in motherese in tone and stress languages. Infant Behav Dev 14: 415-440. doi:10.1016/0163-6383(91)90031-M.
.. [21] Slaney M, McRoberts G (2003) BabyEars: A recognition system for affective vocalizations. Speech Commun 39: 367-384. doi:10.1016/ S0167-6393(02)00049-3.
.. [22] Trainor LJ, Austin CM, Desjardins RN (2000) Is infant-directed speech prosody a result of the vocal expression of emotion? Psychol Sci 11: 188-195. doi:10.1111/1467-9280.00240. PubMed: 11273402.
.. [23] Nwokah EE, Hsu HC, Davies P, Fogel A (1999) The integration of laughter and speech in vocal communication: a dynamic systems perspective. J Speech Lang Hear Res 42: 880-894. PubMed: 10450908.
.. [24] Burnham D, Kitamura C, Vollmer-Conna U (2002) What's new, pussycat? On talking to babies and animals. Science 296: 1435-1435.  doi:10.1126/science.1069587. PubMed: 12029126.
.. [25] Swanson LA, Leonard LB, Gandour J (1992) Vowel duration in mothers' speech to young children. J Speech Hear Res 35: 617-625.  PubMed: 1608253.
.. [26] Swanson LA, Leonard LB (1994) Duration of function-word vowels in mothers' speech to young children. J Speech Hear Res 37: 1394-1405.  PubMed: 7877296.
.. [27] Fernald A, Mazzie C (1991) Prosody and focus in speech to infants and adults. Dev Psychol 27: 209-221. doi:10.1037/0012-1649.27.2.209.
.. [28] Albin DD, Echols CH (1996) Stressed and word-final syllables in infantdirected speech. Infant Behav Dev 19: 401-418. doi:10.1016/ S0163-6383(96)90002-8.
.. [29] Inoue T, Nakagawa R, Kondou M, Koga T, Shinohara K (2011) Discrimination between mothers' infant and adult-directed speech using hidden Markov models. Neurosci Res, 70: 62–70. PubMed: 21256898.
.. [30] Mahdhaoui A, Chetouani M, Cassel RS, Saint-Georges C, Parlato E, et al. (2011) Computerized home video detection for motherese may help to study impaired interaction between infants who become autistic and their parents. Int J Methods Psychiatr Res 20: e6-e18. doi:10.1002/mpr.  332. PubMed: 21574205.
.. [31] Cristià A (2010) Phonetic enhancement of sibilants in infant-directed speech. J Acoust Soc Am 128: 424-434. doi:10.1121/1.3436529.  PubMed: 20649236.
.. [32] Englund K, Behne D (2006) Changes in Infant Directed Speech in the First Six Months. Infant Child Dev 15: 139-160. doi:10.1002/icd.445.
.. [33] Englund KT, Behne DM (2005) Infant directed speech in natural interaction--Norwegian vowel quantity and quality. J Psycholinguist Res 34: 259-280. doi:10.1007/s10936-005-3640-7. PubMed: 16050445.
.. [34] Englund K (2005) Voice onset time in infant directed speech over the first six months.  First Lang 25: 219-234.  doi: 10.1177/0142723705050286.
.. [35] Green JR, Nip IS, Wilson EM, Mefferd AS, Yunusova Y (2010) Lip movement exaggerations during infant-directed speech. J Speech Lang Hear Res 53: 1529-1542. doi:10.1044/1092-4388(2010/09-0005) PubMed: 20699342
.. [36] Lee SA, Davis B, Macneilage P (2010) Universal production patterns and ambient language influences in babbling: a cross-linguistic study of Korean and English-learning infants. J Child Lang England, 37: 293-318. PubMed: 19570317.
.. [37] Shute B, Wheldall K (1989) Pitch alterations in British motherese: some preliminary acoustic data. J Child Lang 16: 503-512. doi:10.1017/ S0305000900010680. PubMed: 2808570.
.. [38] Segal O, Nir-Sagiv B, Kishon-Rabin L, Ravid D (2009) Prosodic patterns in Hebrew child-directed speech. J Child Lang 36: 629-656.  doi:10.1017/S030500090800915X. PubMed: 19006600.
.. [39] Lee S, Davis BL, Macneilage PF (2008) Segmental properties of input to infants: a study of Korean. J Child Lang 35: 591-617. PubMed: 18588716.
.. [40] Grieser DL, Kuhl K (1988) Maternal speech to infants in a tonal language: Support for universal prosodic features in motherese. Dev Psychol 24: 14-20. doi:10.1037/0012-1649.24.1.14.
.. [41] Liu HM, Tsao FM, Kuhl PK (2007) Acoustic analysis of lexical tone in Mandarin infant-directed speech. Dev Psychol 43: 912-917. doi: 10.1037/0012-1649.43.4.912. PubMed: 17605524.
.. [42] Fais L, Kajikawa S, Amano S, Werker JF (2010) Now you hear it, now you don't: vowel devoicing in Japanese infant-directed speech. J Child Lang England, 37: 319-340. PubMed: 19490747.
.. [43] Masataka N (1992) Motherese in a signed language. Infant Behav Dev 15: 453-460. doi:10.1016/0163-6383(92)80013-K.
.. [44] Masataka N (1998) Perception of motherese in Japanese sign language by 6-month-old hearing infants. Dev Psychol 34: 241-246.  doi:10.1037/0012-1649.34.2.241. PubMed: 9541776.
.. [45] Reilly JS, Bellugi U (1996) Competition on the face: affect and language in ASL motherese. J Child Lang 23: 219-239. PubMed: 8733568.
.. [46] Werker JF, Pons F, Dietrich C, Kajikawa S, Fais L et al. (2007) Infantdirected speech supports phonetic category learning in English and Japanese.  Cognition 103: 147-162.  doi:10.1016/j.cognition.  2006.03.006. PubMed: 16707119.
.. [47] Kitamura C, Burnham D (2003) Pitch and communicative intent in mother's speech: Adjustments for age and sex in the first year. Infancy 4: 85-110. doi:10.1207/S15327078IN0401_5.
.. [48] Stern DN, Spieker S, Barnett RK, MacKain K (1983) The prosody of maternal speech: infant age and context related changes. J Child Lang 10: 1-15. PubMed: 6841483.
.. [49] Liu HM, Tsao FM, Kuhl PK (2009) Age-related changes in acoustic modifications of Mandarin maternal speech to preverbal infants and five-year-old children: a longitudinal study. J Child Lang 36: 909-922.  doi:10.1017/S030500090800929X. PubMed: 19232142.
.. [50] Niwano K, Sugai K (2002) Intonation contour of Japanese maternal infant-directed speech and infant vocal response. Jpn J Spec Educ 39: 59-68.
.. [51] Kajikawa S, Amano S, Kondo T (2004) Speech overlap in Japanese mother-child conversations. J Child Lang 31: 215-230. doi:10.1017/ S0305000903005968. PubMed: 15053091.
.. [52] Amano S, Nakatani T, Kondo T (2006) Fundamental frequency of infants' and parents' utterances in longitudinal recordings. J Acoust Soc Am 119: 1636-1647. doi:10.1121/1.2161443. PubMed: 16583908.
.. [53] Snow (1972) Mother's speech to children learning language. Child Dev 43: 549-565. doi:10.2307/1127555.
.. [54] Kitamura C, Thanavishuth C, Burnham D, Luksaneeyanawin S (2002) Universality and specificity in infant-directed speech: Pitch modifications as a function of infant age and sex in a tonal and nontonal language. Infant Behav Dev 24: 372-392.
.. [55] Feldman R (2007) Parent–infant synchrony and the construction of shared timing; physiological precursors, developmental outcomes, and risk conditions. J Child Psychol Psychiatry 48: 329-354. doi:10.1111/j.  1469-7610.2006.01701.x. PubMed: 17355401.
.. [56] Gleason JB (1977) Talking to children: some notes on feed-back. In: F Sa. Talking to Children: Language Input and Acquisition. Cambridge, UK: Cambridge University Press.
.. [57] Braarud HC, Stormark KM (2008) Prosodic modification and vocal adjustments in mothers' speech during face-to-face interaction with their two to four-month-old infants: A double video study. Soc Dev 17: 1074-1084. doi:10.1111/j.1467-9507.2007.00455.x.
.. [58] Smith NA, Trainor LJ (2008) Infant-directed speech is modulated by infant feedback.  Infancy 13: 410-420.  doi: 10.1080/15250000802188719.
.. [59] Shimura Y, Yamanoucho I (1992) Sound spectrographic studies on the relation between motherese and pleasure vocalization in early infancy.  Acta Paediatr Jpn 34: 259-266.  doi:10.1111/j.1442-200X.  1992.tb00956.x. PubMed: 1509871.
.. [60] Van Puyvelde M, Vanfleteren P, Loots G, Deschuyffeleer S, Vinck B et al. (2010) Tonal synchrony in mother-infant interaction based on harmonic and pentatonic series. Infant Behav Dev 33: 387-400. doi: 10.1016/j.infbeh.2010.04.003. PubMed: 20478620.
.. [61] McRoberts GW, Best CT (1997) Accommodation in mean f0 during mother-infant and father-infant vocal interactions: a longitudinal case study. J Child Lang 24: 719-736. doi:10.1017/S030500099700322X.  PubMed: 9519592.
.. [62] Reissland N, Stephenson T (1999) Turn-taking in early vocal interaction: a comparison of premature and term infants' vocal interaction with their mothers. Child Care Health Dev 25: 447-456. doi: 10.1046/j.1365-2214.1999.00109.x. PubMed: 10547707.
.. [63] Niwano K, Sugai K (2003) Maternal accommodation in infant-directed speech during mother's and twin-infants' vocal interactions. Psychol Rep 92: 481-487. doi:10.2466/pr0.2003.92.2.481. PubMed: 12785629.
.. [64] Reissland N, Shepherd J, Cowie L (2002) The melody of surprise: maternal surprise vocalizations during play with her infant. Infant Child Dev 11: 271-278. doi:10.1002/icd.258.
.. [65] Lederberg AR (1984) Interaction between deaf preschoolers and unfamiliar hearing adults. Child Dev 55: 598-606. doi:10.2307/1129971.  PubMed: 6723449.
.. [66] Fidler DJ (2003) Parental vocalizations and perceived immaturity in down syndrome. Am J Ment Retard 108: 425-434. doi: 10.1352/0895-8017(2003)108. PubMed: 14561106.
.. [67] Gogate LJ, Bahrick LE, Watson JD (2000) A study of multimodal motherese: The role of temporal synchrony between verbal labels and gestures. Child Dev 71: 878-894. doi:10.1111/1467-8624.00197.  PubMed: 11016554.
.. [68] Kavanaugh RD, Jirkovsky AM (1982) Parental speech to young children: A longitudinal analysis. Merrill-Palmer. Q: J of Dev Psych 28: 297-311.
.. [69] Bohannon JN, Marquis AL (1977) Children's control of adult speech.  Child Dev 48: 1002-1008. doi:10.2307/1128352.
.. [70] Bergeson TR, Miller RJ, McCune K (2006) Mothers' Speech to Hearing-Impaired Infants and Children With Cochlear Implants. Infancy 10: 221-240. doi:10.1207/s15327078in1003_2.
.. [71] Kondaurova MV, Bergeson TR (2010) The effects of age and infant hearing status on maternal use of prosodic cues for clause boundaries in speech. J Speech Lang Hear Res
.. [72] Ikeda Y, Masataka N (1999) A variable that may affect individual differences in the child-directed speech of Japanese women. Jpn Psychol Res 41: 203-208. doi:10.1111/1468-5884.00120.
.. [73] Hoff E, Tian C (2005) Socioeconomic status and cultural influences on language. J Commun Disord 38: 271-278. doi:10.1016/j.jcomdis.  2005.02.003. PubMed: 15862810.
.. [74] Hoff-Ginsberg E (1991) Mother-child conversation in different social classes and communicative settings. Child Dev 62: 782-796. doi: 10.2307/1131177. PubMed: 1935343.
.. [75] Rowe ML (2008) Child-directed speech: relation to socioeconomic status, knowledge of child development and child vocabulary skill. J Child Lang 35: 185-205. PubMed: 18300434.
.. [76] Matsuda YT, Ueno K, Waggoner RA, Erickson D, Shimura Y et al.  (2011) Processing of infant-directed speech by adults. NeuroImage 54: 611-621. doi:10.1016/j.neuroimage.2010.07.072. PubMed: 20691794.
.. [77] Gordon I, Zagoory-Sharon O, Leckman JF, Feldman R (2010) Oxytocin and the development of parenting in humans. Biol Psychiatry 68: 377-382. doi:10.1016/j.biopsych.2010.02.005. PubMed: 20359699.
.. [78] Bettes BA (1988) Maternal depression and motherese: temporal and intonational features. Child Dev 59: 1089-1096. doi:10.2307/1130275.  PubMed: 3168616.
.. [79] Herrera E, Reissland N, Shepherd J (2004) Maternal touch and maternal child-directed speech: effects of depressed mood in the postnatal period. J Affect Disord 81: 29-39. doi:10.1016/j.jad.  2003.07.001. PubMed: 15183597.
.. [80] Kaplan PS, bachorowski J-A, Smoski MJ, Zinser M (2001) Role of clinical diagnosis and medication use in effects of maternal depression on infant-directed speech. Infancy 2: 537-548. doi:10.1207/ S15327078IN0204_08.
.. [81] Wan MW, Penketh V, Salmon MP, Abel KM (2008) Content and style of speech from mothers with schizophrenia towards their infants.  Psychiatry Res 159: 109-114. doi:10.1016/j.psychres.2007.05.012.  PubMed: 18329722.
.. [82] McLeod PJ (1993) What studies of communication with infants ask us about psychology: Baby-talk and other speech registers. Canadian Psychology/Psychologie canadienne 34: 282-292
.. [83] Fernald A (1993) Approval and disapproval: Infant responsiveness to vocal affect in familiar and unfamiliar languages. Child Dev 64: 657-674. doi:10.2307/1131209. PubMed: 8339687.
.. [84] Papoušek M, Bornstein MH, Nuzzo C, Papoušek H (1990) Infant responses to prototypical melodic contours in parental speech. Infant Behav Dev 13: 539-545. doi:10.1016/0163-6383(90)90022-Z.
.. [85] Elena S, Merideth G (2012) Infants infer intentions from prosody. Cogn Dev 27: 1-16. doi:10.1016/j.cogdev.2011.08.003.
.. [86] Santesso DL, Schmidt LA, Trainor LJ (2007) Frontal brain electrical activity (EEG) and heart rate in response to affective infant-directed (ID) speech in 9-month-old infants. Brain Cogn 65: 14-21. doi:10.1016/ j.bandc.2007.02.008. PubMed: 17659820.
.. [87] Monnot M (1999) Function of infant-directed speech. Hum Nat 10: 415-443. doi:10.1007/s12110-999-1010-0.
.. [88] Fernald A, Kuhl P (1987) Acoustic determinants of infant preference for motherese speech. Infant Behav Dev 10: 279-293. doi: 10.1016/0163-6383(87)90017-8.
.. [89] Santarcangelo S, Dyer K (1988) Prosodic aspects of motherese: effects on gaze and responsiveness in developmentally disabled children. J Exp Child Psychol 46: 406-418. doi:10.1016/0022-0965(88)90069-0.  PubMed: 3216186.
.. [90] Schachner A, Hannon EE (2010) Infant-directed speech drives social preferences in 5-month-old infants. Dev Psychol, 47: 19–25. PubMed: 20873920.
.. [91] Werker JF, McLeod PJ (1989) Infant preference for both male and female infant-directed talk: a developmental study of attentional and affective responsiveness. Can J Psychol 43: 230-246. doi:10.1037/ h0084224. PubMed: 2486497.
.. [92] Cooper RP (1993) The effect of prosody on young infants' speech perception. Advances Infancy Res 8: 137-167.
.. [93] Cooper RP, Aslin RN (1990) Preference for infant-directed speech in the first month after birth. Child Dev 61: 1584-1595. doi: 10.2307/1130766. PubMed: 2245748.
.. [94] Pegg JE, Werker JF, McLeod PJ (1992) Preference for infant-directed over adult-directed speech: Evidence from 7-week-old infants. Infant Behav Dev 15: 325-345. doi:10.1016/0163-6383(92)80003-D.
.. [95] Saito Y, Aoyama S, Kondo T, Fukumoto R, Konishi N et al. (2007) Frontal cerebral blood flow change associated with infant-directed speech. Arch Dis Child Fetal Neonatal Ed England, 92: F113-F116.  PubMed: 16905571.
.. [96] Hayashi A, Tamekawa Y, Kiritani S (2001) Developmental change in auditory preferences for speech stimuli in Japanese infants. J Speech Lang Hear Res 44: 1189-1200. doi:10.1044/1092-4388(2001/092).  PubMed: 11776357.
.. [97] Newman RS, Hussain I (2006) Changes in Preference for InfantDirected Speech in Low and Moderate Noise by 4.5 to 13-Month-Olds.  Infancy 10: 61-76. doi:10.1207/s15327078in1001_4.
.. [98] Cooper RP, Abraham J, Berman S, Staska M (1997) The development of infants' preference for motherese. Infant Behav Dev 20: 477-488.  doi:10.1016/S0163-6383(97)90037-0.
.. [99] Hepper PG, Scott D, Shahidullah S (1993) Newborn and fetal response to maternal voice. J Reprod Infant Psychol 11: 147-153. doi: 10.1080/02646839308403210.
.. [100] Panneton R, Kitamura C, Mattock K, Burnham D (2006) Slow Speech Enhances Younger But Not Older Infants' Perception of Vocal Emotion.  Res Hum Dev 3: 7-19. doi:10.1207/s15427617rhd0301_2.
.. [101] Niwano K, Sugai K (2002) Acoustic determinants eliciting Japanese infants' vocal response to maternal speech. Psychol Rep 90: 83-90.  doi:10.2466/pr0.2002.90.1.83. PubMed: 11899017.
.. [102] Kitamura C, Lam C (2009) Age-specific preferences for infant-directed affective intent. Infancy 14: 77-100. doi:10.1080/15250000802569777.
.. [103] Kaplan PS, Owren MJ (1994) Dishabituation of visual attention in 4month-olds by infant-directed frequency sweeps. Infant Behav Dev 17: 347-358. doi:10.1016/0163-6383(94)90027-2.
.. [104] Spence MJ, Moore DS (2003) Categorization of infant-directed speech: development from 4 to 6 months. Dev Psychobiol 42: 97-109. doi: 10.1002/dev.10093. PubMed: 12471640.
.. [105] Johnson DM, Dixon DR, Coon RC, Hilker K, Gouvier WD (2002) Watch what you say and how you say it: differential response to speech by participants with and without head injuries. Appl Neuropsychol 9: 58-62.  doi:10.1207/S15324826AN0901_7. PubMed: 12173751.
.. [106] Cooper RP, Aslin RN (1994) Developmental differences in infant attention to the spectral properties of infant-directed speech. Child Dev 65: 1663-1677. doi:10.2307/1131286. PubMed: 7859548.
.. [107] Leibold LJ, Werner LA (2007) Infant auditory sensitivity to pure tones and frequency-modulated tones. Infancy 12: 225-233. doi:10.1111/j.  1532-7078.2007.tb00241.x.
.. [108] Trainor LJ, Zacharias CA (1998) Infants prefer higher-pitched singing.  Infant Behav Dev 21: 799-805. doi:10.1016/S0163-6383(98)90047-9.
.. [109] Colombo J, Frick JE, Ryther JS, Coldren JT (1995) Infants' detection of analogs of 'motherese' in noise. Merrill-Palmer. Q: J of Dev Psych 41: 104-113.
.. [110] Singh L, Morgan JL, Best CT (2002) Infants' Listening Preferences: Baby Talk or Happy Talk? Infancy 3: 365-394. doi:10.1207/ S15327078IN0303_5.
.. [111] McRoberts GW, McDonough C, Lakusta L (2009) The role of verbal repetition in the development of infant speech preferences from 4 to 14 months of age. Infancy 14: 162-194. doi:10.1080/15250000802707062.
.. [112] Nakata T, Trehub SE (2004) Infants' responsiveness to maternal speech and singing. Infant Behav Dev 27: 455-464. doi:10.1016/ j.infbeh.2004.03.002.
.. [113] Trehub SE, Nakata T (2001) Emotion and music in infancy. Musicae Sci Spec Issue: 2001-2002: 37-61
.. [114] Kaplan PS, Goldstein MH, Huckeby ER, Cooper RP (1995) Habituation, sensitization, and infants' responses to motherese speech.  Dev Psychobiol 28: 45-57. doi:10.1002/dev.420280105. PubMed: 7895923.
.. [115] Kaplan PS, Jung PC, Ryther JS, Zarlengo-Strouse P (1996) Infantdirected versus adult-directed speech as signals for faces. Dev Psychol 32: 880-891. doi:10.1037/0012-1649.32.5.880.
.. [116] Senju A, Csibra G (2008) Gaze following in human infants depends on communicative signals. Curr Biol 18: 668-671. doi:10.1016/j.cub.  2008.03.059. PubMed: 18439827.
.. [117] Gergely G, Egyed K, Király I (2007) On pedagogy. Dev Sci 10: 139-146. doi:10.1111/j.1467-7687.2007.00576.x. PubMed: 17181712.
.. [118] Kaplan PS, Bachorowski JA, Smoski MJ, Hudenko WJ (2002) Infants of depressed mothers, although competent learners, fail to learn in response to their own mothers' infant-directed speech. Psychol Sci 13: 268-271. doi:10.1111/1467-9280.00449. PubMed: 12009049.
.. [119] Kaplan PS, Bachorowski JA, Zarlengo-Strouse P (1999) Child-directed speech produced by mothers with symptoms of depression fails to promote associative learning in 4-month-old infants. Child Dev 70: 560-570. doi:10.1111/1467-8624.00041. PubMed: 10368910.
.. [120] Kaplan PS, Danko CM, Diaz A, Kalinka CJ (2010) An associative learning deficit in 1-year-old infants of depressed mothers: Role of depression duration. Infant Behav Dev.
.. [121] Kaplan PS, Dungan JK, Zinser MC (2004) Infants of chronically depressed mothers learn in response to male, but not female, infantdirected speech.  Dev Psychol 40: 140-148.  doi: 10.1037/0012-1649.40.2.140. PubMed: 14979756.
.. [122] Kaplan PS, Danko CM, Diaz A (2010) A Privileged Status for Male Infant-Directed Speech in Infants of Depressed Mothers? Role of Father Involvement.  Infancy 15: 151-175.  doi:10.1111/j.  1532-7078.2009.00010.x.
.. [123] Kaplan PS, Sliter JK, Burgess AP (2007) Infant-directed speech produced by fathers with symptoms of depression: effects on infant associative learning in a conditioned-attention paradigm. Infant Behav Dev 30: 535-545. doi:10.1016/j.infbeh.2007.05.003. PubMed: 17604106.
.. [124] Kaplan PS, Burgess AP, Sliter JK, Moreno AJ (2009) Maternal Sensitivity and the Learning-Promoting Effects of Depressed and NonDepressed Mothers' Infant-Directed Speech. Infancy 14: 143-161. doi: 10.1080/15250000802706924. PubMed: 20046973.
.. [125] Karzon RG (1985) Discrimination of polysyllabic sequences by one to four-month-old infants. J Exp Child Psychol 39: 326-342. doi: 10.1016/0022-0965(85)90044-X. PubMed: 3989467.
.. [126] Karzon RG, Nicholas JG (1989) Syllabic pitch perception in 2 to 3month-old infants. Percept Psychophys 45: 10-14. doi:10.3758/ BF03208026. PubMed: 2913563.
.. [127] Vallabha GK, McClelland JL, Pons F, Werker JF, Amano S (2007) Unsupervised learning of vowel categories from infant-directed speech.  Proc Natl Acad Sci U S A 104: 13273-13278. doi:10.1073/pnas.  0705369104. PubMed: 17664424.
.. [128] Trainor LJ, Desjardins RN (2002) Pitch characteristics of infant-directed speech affect infants' ability to discriminate vowels. Psychon Bull Rev 9: 335-340. doi:10.3758/BF03196290. PubMed: 12120797.
.. [129] Hirsh-Pasek K, Kemler Nelson DG, Jusczyk PW, Cassidy KW (1987) Clauses are perceptual units for young infants. Cognition 26: 269-286.  doi:10.1016/S0010-0277(87)80002-1. PubMed: 3677573.
.. [130] Kemler Nelson DG, Hirsh-Pasek K, Jusczyk PW, Cassidy KW (1989) How the prosodic cues in motherese might assist language learning. J Child Lang 16: 55-68. doi:10.1017/S030500090001343X. PubMed: 2925815.
.. [131] Thiessen ED, Hill EA, Saffran JR (2005) Infant-Directed Speech Facilitates Word Segmentation. Infancy 7: 53-71. doi:10.1207/ s15327078in0701_5.
.. [132] D'Odorico L, Jacob V (2006) Prosodic and lexical aspects of maternal linguistic input to late-talking toddlers. Int J Lang Commun Disord 41: 293-311. doi:10.1080/13682820500342976. PubMed: 16702095.
.. [133] Morgan JL (1996) Prosody and the roots of parsing. Lang Cogn Processes 11: 69-106. doi:10.1080/016909696387222.
.. [134] Curtin S, Mintz TH, Christiansen MH (2005) Stress changes the representational landscape: evidence from word segmentation.  Cognition 96: 233-262. doi:10.1016/j.cognition.2004.08.005. PubMed: 15996560.
.. [135] Bortfeld H, Morgan JL (2010) Is early word-form processing stress-full?  How natural variability supports recognition. Cogn Psychol 60: 241-266.  doi:10.1016/j.cogpsych.2010.01.002. PubMed: 20159653.
.. [136] Singh L (2008) Influences of high and low variability on infant word recognition.  Cognition 106: 833-870.  doi:10.1016/j.cognition.  2007.05.002. PubMed: 17586482.
.. [137] Kirchhoff K, Schimmel S (2005) Statistical properties of infant-directed versus adult-directed speech: insights from speech recognition. J Acoust Soc Am 117: 2238-2246. doi:10.1121/1.1869172. PubMed: 15898664.
.. [138] Singh L, Nestor S, Parikh C, Yull A (2009) Influences of infant-directed speech on early word recognition. Infancy 14: 654-666. doi: 10.1080/15250000903263973.
.. [139] Song JY, Demuth K, Morgan J (2010) Effects of the acoustic properties of infant-directed speech on infant word recognition. J Acoust Soc Am 128: 389-400. doi:10.1121/1.3419786. PubMed: 20649233.
.. [140] Bard EG, Anderson AH (1983) The unintelligibility of speech to children. J Child Lang 10: 265-292. PubMed: 6874768.
.. [141] Zangl R, Mills DL (2007) Increased Brain Activity to Infant-Directed Speech in 6-and 13-Month-Old Infants. Infancy 11: 31-62. doi:10.1207/ s15327078in1101_2.
.. [142] Newport E, Gleitman H, Gleitman L (1977) Mother, I'd rather do it myself: Some effects and non-effects of maternal speech style. . In: CSaC, Ferguson. Talking to Children: Language Input and Acquisition.  Cambridge: Cambridge University Press.
.. [143] Furrow D, Nelson K, Benedict H (1979) Mothers' speech to children and syntactic development: Some simple relationships. J Child Lang 6: 423-442. PubMed: 536408.
.. [144] Gleitman LR, Newport EL, Gleitman H (1984) The current status of the motherese hypothesis. J Child Lang 11: 43-79. PubMed: 6699113.
.. [145] Scarborough H, Wyckoff J (1986) Mother, I'd still rather do it myself: some further non-effects of 'motherese'. J Child Lang 13: 431-437.  PubMed: 3745343.
.. [146] Furrow D, Nelson K (1986) A further look at the motherese hypothesis: a reply to Gleitman, Newport & Gleitman. J Child Lang 13: 163-176.  PubMed: 3949896.
.. [147] Hampson J, Nelson K (1993) The relation of maternal language to variation in rate and style of language acquisition. J Child Lang 20: 313-342. PubMed: 8376472.
.. [148] Waterfall HR, Sandbank B, Onnis L, Edelman S (2010) An empirical generative framework for computational modeling of language acquisition.  J Child Lang 37: 671-703.  doi:10.1017/ S0305000910000024. PubMed: 20420744.
.. [149] Onnis L, Waterfall HR, Edelman S (2008) Learn locally, act globally: learning language from variation set cues. Cognition 109: 423-430. doi: 10.1016/j.cognition.2008.10.004. PubMed: 19019350.
.. [150] Fernald A, Hurtado N (2006) Names in frames: infants interpret words in sentence frames faster than words in isolation. Dev Sci 9: F33-F40.  doi:10.1111/j.1467-7687.2005.00460.x. PubMed: 16669790.
.. [151] Golinkoff RM, Alioto A (1995) Infant-directed speech facilitates lexical learning in adults hearing Chinese: implications for language acquisition. J Child Lang 22: 703-726. PubMed: 8789520.
.. [152] Kempe V, Brooks PJ, Gillis S (2005) Diminutives in child-directed speech supplement metric with distributional word segmentation cues.  Psychon Bull Rev 12: 145-151. doi:10.3758/BF03196360. PubMed: 15945207.
.. [153] Kempe V, Brooks PJ, Gillis S, Samson G (2007) Diminutives facilitate word segmentation in natural speech: cross-linguistic evidence. Mem Cogn 35: 762-773. doi:10.3758/BF03193313. PubMed: 17848033.
.. [154] Kempe V, Brooks PJ, Mironova N, Fedorova O (2003) Diminutivization supports gender acquisition in Russian children. J Child Lang 30: 471-485. doi:10.1017/S0305000903005580. PubMed: 12846306.
.. [155] Seva N, Kempe V, Brooks PJ, Mironova N, Pershukova A et al. (2007) Crosslinguistic evidence for the diminutive advantage: gender agreement in Russian and Serbian children. J Child Lang 34: 111-131.  doi:10.1017/S0305000906007720. PubMed: 17340940.
.. [156] Hayes DP, Ahrens MG (1988) Vocabulary simplification for children: a special case of 'motherese'? J Child Lang 15: 395-410. doi:10.1017/ S0305000900012411. PubMed: 3209647.
.. [157] Bunce VL, Harrison DW (1991) Child or adult-directed speech and esteem: effects on performance and arousal in elderly adults. Int J Aging Hum Dev 32: 125-134. doi:10.2190/JKA7-15D2-0DFT-U934.  PubMed: 2055658.
.. [158] Hirsh-Pasek K, Treiman R (1982) Doggerel: motherese in a new context. J Child Lang 9: 229-237. PubMed: 7061632.
.. [159] Weppelman TL, Bostow A, Schiffer R, Elbert-Perez E, Newman RS (2003) Children's use of the prosodic characteristics of infant-directed speech.  Lang Commun 23: 63-80.  doi:10.1016/ S0271-5309(01)00023-4.
.. [160] Rice ML, Haight PL (1986) "Motherese" of Mr. Rogers: a description of the dialogue of educational television programs. J Speech Hear Disord 51: 282-287. PubMed: 3736028.
.. [161] Biben M, Symmes D, Bernhards D (1989) Contour variables in vocal communication between squirrel monkey mothers and infants. Dev Psychobiol 22: 617-631. doi:10.1002/dev.420220607. PubMed: 2792572.
.. [162] Snowdon CT, Teie D (2010) Affective responses in tamarins elicited by species-specific music. Biol Lett 6: 30-32. doi:10.1098/rsbl.2009.0593.  PubMed: 19726444.
.. [163] Falk D (2004) Prelinguistic evolution in early hominins: whence motherese? Behav Brain Sci 27: 483-503; discussion: 15773427.
.. [164] Fernald A (1992) Human maternal vocalizations to infants as biologically relevant signals: An evolutionary perspective. In: Barkow, Cosmides, Tooby, editors. The Adapted. Mind: 391—428
.. [165] Iverson JM, Capirci O, Longobardi E, Caselli MC (1999) Gesturing in mother–child interactions. Cogn Dev 14: 57-75. doi:10.1016/ S0885-2014(99)80018-5.
.. [166] O'Neill M, Bard KA, Linnell M, Fluck M (2005) Maternal gestures with 20-month-old infants in two contexts. Dev Sci 8: 352-359. doi:10.1111/j.  1467-7687.2005.00423.x. PubMed: 15985069.
.. [167] Brand RJ, Baldwin DA, Ashburn LA (2002) Evidence for 'motionese': Modifications in mothers' infant-directed action. Dev Sci 5: 72-83. doi: 10.1111/1467-7687.00211.
.. [168] Longhi E (2009) 'Songese': Maternal structuring of musical interaction with infants.  Psychol Music 37: 195-213.  doi: 10.1177/0305735608097042.
.. [169] Fernald A (2000) Speech to infants as hyperspeech: knowledge-driven processes in early word recognition. Phonetica 57: 242-254. doi: 10.1159/000028477. PubMed: 10992144.
.. [170] Matychuk P (2005) The role of child-directed speech in language acquisition: A case study. Lang Sci 27: 301-379. doi:10.1016/j.langsci.  2004.04.004.
.. [171] Monnot M, Orbelo D, Riccardo L, Sikka S, Rossa E (2003) Acoustic analyses support subjective judgments of vocal emotion. Ann N Y Acad Sci 1000: 288-292. PubMed: 14766639.
.. [172] Walker-Andrews AS (1997) Infants' perception of expressive behaviors: differentiation of multimodal information. Psychol Bull 121: 437-456.  doi:10.1037/0033-2909.121.3.437. PubMed: 9136644.
.. [173] Parncutt R (2009) Prenatal and infant conditioning, the mother schema, and the origins of music and religion. Musicae Sci special issue: 119-150.
.. [174] Kuhl PK, Tsao FM, Liu HM (2003) Foreign-language experience in infancy: effects of short-term exposure and social interaction on phonetic learning. Proc Natl Acad Sci U S A United States, 100: 9096-9101. PubMed: 12861072.
.. [175] Kuhl PK (2007) Is speech learning gated by the social brain? Dev Sci 10: 110-120.  doi:10.1111/j.1467-7687.2007.00572.x.  PubMed: 17181708.
.. [176] Papousek H, Papousek M (1983) Biological basis of social interactions: implications of research for an understanding of behavioural deviance.  J Child Psychol Psychiatry 24: 117-129.  doi:10.1111/j.  1469-7610.1983.tb00109.x. PubMed: 6826670.
.. [177] Cohen D, Cassel RS, Saint-Georges C, Mahdhaoui A, Laznik M-C, et al. (2013) Do Parentese Prosody and Fathers' Involvement in Interacting Facilitate Social Interaction in Infants Who Later Develop Autism? PLOS ONE 8: e61402. doi:10.1371/journal.pone.0061402.  PubMed: 23650498.  

.. rubric:: affiliation

.. [#f1] Department of Child and Adolescent Psychiatry, Pitié-Salpêtrière Hospital, Université Pierre et Marie Curie, Paris, France, 
.. [#f2] Institut des Systèmes Intelligents et de Robotique, Centre National de la Recherche Scientifique 7222, Université Pierre et Marie Curie, Paris, France, 
.. [#f3] Laboratoire de Psychopathologie et Processus de Santé (LPPS, EA 4057), Institut de Psychologie de l'Université Paris Descartes, Paris, France, 
.. [#f4] IRCCS Scientific Institute Stella Maris, University of Pisa, Pisa, Italy, 
.. [#f5] Department of Child and Adolescent Psychiatry, Association Santé Mentale du 13ème, Centre Alfred Binet, Paris, France
