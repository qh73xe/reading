.. 読書録 documentation master file, created by
   sphinx-quickstart on Fri Jan 17 08:21:20 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===============================
苦労する遊び人の読書録
===============================

このページは早稲田大学人間科学部、浅井拓也の読書ログです。
基本的には私がこれまで読んできた論文に関しての翻訳メモを記述します。

Contents (論文)
===============================

言語獲得研究
------------------------------

.. toctree::
   :glob:
   :titlesonly:
   :maxdepth: 1

   ./LanguageAcquisition/*/main

非言語的音響特徴
------------------------------

.. toctree::
   :glob:
   :titlesonly:
   :maxdepth: 1

   ./Paralanguage/*/main


R 言語周辺
------------------------------

.. toctree::
   :glob:
   :titlesonly:
   :maxdepth: 1

   R/*/main

書籍
-------------------------------

.. toctree::
   :glob:
   :titlesonly:
   :maxdepth: 1

   Book/*/main

一覧
================================

.. toctree::
   :glob:
   :titlesonly:
   :maxdepth: 1

   */*/main
